const config = require('./src/utils/siteConfig')

const path = require(`path`)

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /react-particles-js/,
            use: loaders.null(),
          },
          {
            test: /simple-react-lightbox/,
            use: loaders.null(),
          },
          {
            test: /react-burger-menu/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  const loadHandwerkSubpages = new Promise(resolve => {
    graphql(`
      {
        allContentfulPage(filter: { category: { eq: "Handwerk" } }) {
          edges {
            node {
              slug
              id
              category
              subtitle
              title
              headerImage {
                id
                fluid(maxWidth: 640) {
                  srcWebp
                  srcSetWebp
                  srcSet
                  src
                  sizes
                  aspectRatio
                }
              }
            }
          }
        }
      }
    `).then(result => {
      const subpages = result.data.allContentfulPage.edges

      // Create each individual post
      subpages.forEach(edge => {
        const pagePath = `/${edge.node.slug.replace('-', '/')}/`
        createPage({
          path: pagePath,
          component: path.resolve(`./src/templates/handwerkSubpage.js`),
          context: {
            slug: edge.node.slug,
          },
        })
      })
      resolve()
    })
  })

  const loadJobs = new Promise(resolve => {
    graphql(`
      {
        allContentfulJob(
          sort: { fields: [publishDate], order: DESC }
          limit: 10000
        ) {
          edges {
            node {
              slug
              publishDate
              heroImage {
                id
                title
                fluid {
                  srcSet
                  src
                  tracedSVG
                  srcWebp
                  srcSetWebp
                }
              }
            }
          }
        }
      }
    `).then(result => {
      const jobs = result.data.allContentfulJob.edges

      // Create main home page
      createPage({
        path: `/jobs/`,
        component: path.resolve(`./src/templates/jobs.js`),
      })

      // Create each individual post
      jobs.forEach((edge, i) => {
        const postPrefix = `/jobs/`
        createPage({
          path: `${postPrefix}${edge.node.slug}/`,
          component: path.resolve(`./src/templates/job.js`),
          context: {
            slug: edge.node.slug,
          },
        })
      })
      resolve()
    })
  })

  return Promise.all([loadHandwerkSubpages, loadJobs])
}
