/* eslint-disable react/prop-types */
import React from 'react'
import { graphql, Link } from 'gatsby'
import config from '../../utils/siteConfig'
import Layout from '../../components/Layout'
import Container from '../../components/Container'
import TextContainer from '../../components/TextContainer'
import PageTitle from '../../components/PageTitle'
import ArrowBox from '../../components/ArrowBox'
import RibbonComponent from '../../components/Ribbon'
import PageBody from '../../components/PageBody'
import SliderComponent from '../../components/Slider.Preise'
import SEO from '../../components/SEO'

const Kontakt = ({ data }) => {
  const postNode = {
    title: `${data.contentfulPage.title} - ${config.siteTitle}`,
  }

  const { body, title, subtitle, headerImage } = data.contentfulPage

  return (
    <Layout>
      <SEO postNode={postNode} pagePath="/preise" customTitle />

      <TextContainer>
        <Link to="/preise/rechner" className="conv_preisrechner">
          <RibbonComponent
            text="Sofort-Preisrechner starten."
            link="/preise/rechner"
            image={data.contentfulPage.ribbon}
            color="white"
            isCentered
          />
        </Link>
      </TextContainer>

      <PageTitle
        background={headerImage}
        objectFit="contain"
        imgStyles={{
          objectFit: 'contain',
          objectPosition: '50% 50%',
          height: '100%',
        }}
        backgroundColor="#C2CF00"
      />

      <Container style={{ paddingBottom: '0' }}>
        <TextContainer>
          <h1>{title}</h1>
          <h2>{subtitle}</h2>
        </TextContainer>
        {body && <PageBody body={body} color="green" />}
      </Container>

      <Container noPaddingTop>
        <TextContainer>
          <Link to="/preise/rechner" className="conv_preisrechner">
            <ArrowBox className="grey third centered">in Echtzeit</ArrowBox>
            <ArrowBox className="grey third centered">Gratis</ArrowBox>
            <ArrowBox className="grey third centered">
              in einer Minute zum Preis
            </ArrowBox>
            <ArrowBox className="green centered relaxed">
              Jetzt individuellen Preis berechnen
            </ArrowBox>
          </Link>
          {/* eslint-disable-next-line react/prop-types */}
          <SliderComponent
            slidesPerPage={4}
            data={data.allContentfulRichtpreise}
          />
        </TextContainer>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    allContentfulRichtpreise(
      filter: { category: { in: "richtpreise-hauptseite" } }
    ) {
      edges {
        node {
          id
          title
          text
          image {
            fluid(
              quality: 100
              maxWidth: 3840
              resizingBehavior: NO_CHANGE
              toFormat: NO_CHANGE
            ) {
              srcWebp
              srcSetWebp
              srcSet
              src
              sizes
              aspectRatio
              ...GatsbyContentfulFluid
            }
            description
          }
          slug
          price
        }
      }
    }
    contentfulPage(slug: { eq: "preise" }) {
      id
      title
      subtitle
      ribbon {
        fluid {
          srcWebp
          srcSetWebp
          srcSet
          src
          sizes
          aspectRatio
          ...GatsbyContentfulFluid
        }
      }
      headerImage {
        id
        fluid(
          quality: 100
          maxWidth: 3840
          resizingBehavior: NO_CHANGE
          toFormat: NO_CHANGE
        ) {
          srcWebp
          srcSetWebp
          srcSet
          src
          sizes
          aspectRatio
          ...GatsbyContentfulFluid
        }
      }
      body {
        json
      }
    }
  }
`

export default Kontakt
