import React from 'react'
import styled from 'styled-components'
import config from '../../utils/siteConfig'
import Layout from '../../components/Layout'
import Iframe from '../../components/Iframe'
import SEO from '../../components/SEO'

const Rechner = () => {
  const postNode = {
    title: `Preisrechner - ${config.siteTitle}`,
  }

  const StyledDiv = styled.div`
    background: linear-gradient(180deg, #fff 64%, #c1cf00 98%);
    background-image: linear-gradient(
      rgb(255, 255, 255) 64%,
      rgb(193, 207, 0) 98%
    );
    min-height: 62vh;
    padding: 5rem 0 1.5rem 0;
    iframe {
      max-width: 1050px;
      margin: 0 auto;
      display: block;
    }
  `

  return (
    <Layout>
      <SEO postNode={postNode} pagePath="preise/rechner" customTitle />
      <StyledDiv>
        <Iframe mode="taggedElement" src="https://vermoffert.ch/handwerk" />
      </StyledDiv>
    </Layout>
  )
}

export default Rechner
