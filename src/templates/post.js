import React from 'react'
import {graphql} from 'gatsby'
import Helmet from 'react-helmet'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import Container from '../components/Container'
import TextContainer from '../components/TextContainer'
import PageTitle from '../components/PageTitle'
// import PostGalleryGrid from '../components/PostGrid'
// import PageBody from '../components/PageBody'
// import PostDetails from '../components/PostDetails'
import SEO from '../components/SEO'

import 'semantic-ui-css/components/table.min.css'
import 'semantic-ui-css/components/grid.min.css'

const PostTemplate = ({ data, pageContext }) => {
  const {
    title,
    subtitle,
    slug,
    gallery,
    heroImage,
    body,
    tags,
  } = data.contentfulPost
  const postNode = data.contentfulPost

  return (
    <Layout>
      <Helmet>
        <title>{`${title} - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO pagePath={slug} postNode={postNode} postSEO />

      {/*<PageTitle
        isPost
        background={heroImage}
        // title={title}
        // subTitle={subtitle}
      />*/}

      <Container>
        <TextContainer>
          <h1>{title}</h1>
          <h2>{subtitle}</h2>
        </TextContainer>
        {/*<TagList tags={tags} />*/}
        {/*<PageBody body={body} />*/}
        <TextContainer>
          <p>Incentivization social currency and to be inspired is to become creative, innovative and energized we want this philosophy to trickle down to all our stakeholders. Vec let's put a pin in that optics we are running out of runway or deliverables knowledge is power and quarterly sales are at an all-time low. Strategic high-level 30,000 ft view.</p>
          <div className="ui stackable two column grid">
            <div className="column">
              <h3>Was du mitbringst</h3>
              <p>We want to empower the team with the right tools and guidance to uplevel our craft and build better productize workflow ecosystem and work or define the underlying principles that drive decisions and strategy for your design language even dead cats bounce shelfware. Lean into that problem . New economy what's our go to market strategy? Bob called an all-hands this afternoon optics, you gotta smoke test your hypothesis optics yet win-win.</p>
            </div>
            <div className="column">
              <h3>Die Details</h3>
              <table className={'ui basic table'}>
                <tr>
                  <td>Pensum:</td>
                  <td>Vollzeit</td>
                </tr>
                <tr>
                  <td>Arbeitsort:</td>
                  <td>St. Gallen</td>
                </tr>
                <tr>
                  <td>Qualifikation:</td>
                  <td>Fachhochschule oder ähnlich</td>
                </tr>
                <tr>
                  <td>Start:</td>
                  <td>01.01.2021</td>
                </tr>
                <tr>
                  <td className={'active'} colSpan={2} style={{textAlign: 'center'}}>
                    <i className={'icon file pdf'} /><a href={"#"}>Ausschreibung als PDF herunterladen</a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <h3>Interesse geweckt?</h3>
          <p>
            Melde dich bei uns via E-Mail und wir nehmen so bald wie möglich Kontakt mit dir auf. Bitte sende uns deine Bewerbung inklusive Lebenslauf an: <a href={"mailto: jobs@vermflex.ch"}>jobs@vermflex.ch</a>. Wir freuen uns darauf, von dir zu hören!
          </p>
        </TextContainer>

      </Container>

      {/*<PostGalleryGrid items={gallery} columnWidth="33.33%" title="Galerie" />*/}

    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulPage(slug: { eq: "tag" }) {
      id
      title
      subtitle
      headerImage {
        id
        fluid(quality: 100, maxWidth: 1920, resizingBehavior: NO_CHANGE, toFormat: NO_CHANGE) {
          srcWebp
          srcSetWebp
          srcSet
          src
          sizes
          aspectRatio
          ...GatsbyContentfulFluid
        }
      }
      body {
        json
      }
    }
    contentfulPost(slug: { eq: $slug }) {
      title
      slug
      metaDescription {
        internal {
          content
        }
      }
      publishDate(formatString: "MMMM DD, YYYY")
      publishDateISO: publishDate(formatString: "DD-MM-YYYY")
      tags {
        title
        id
        slug
      }
      heroImage {
        title
        fluid(quality: 100, maxWidth: 1920, resizingBehavior: NO_CHANGE, toFormat: NO_CHANGE) {
          srcWebp
          srcSetWebp
          srcSet
          src
          sizes
          aspectRatio
          ...GatsbyContentfulFluid
        }
        ogimg: resize(width: 1280) {
          src
          width
          height
        }
      }
      body {
        json
      }
      gallery {
        id
        contentful_id
        title
        fluid(quality: 100, maxWidth: 3840, resizingBehavior: NO_CHANGE, toFormat: NO_CHANGE) {
          ...GatsbyContentfulFluid
        }
      }
    }
  }
`

export default PostTemplate
