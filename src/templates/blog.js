import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import Layout from '../components/Layout'
import CardList from '../components/CardList'
import Card from '../components/Card'
import Container from '../components/Container'
import Pagination from '../components/Pagination'
import SEO from '../components/SEO'
import config from '../utils/siteConfig'
import TextContainer from "../components/TextContainer";
import PageBody from "../components/PageBody";

const StyledDiv = styled.div`
  max-width: ${props => props.theme.sizes.maxWidth};
  margin: 0 auto;
`

const Blog = ({ data, pageContext }) => {
  const posts = data.allContentfulPost.edges
  const { body, title, subtitle, headerImage } = data.contentfulPage
  const { currentPage } = pageContext
  const isFirstPage = currentPage === 1

  return (
    <Layout>
      <SEO />
      {!isFirstPage && (
        <Helmet>
          <title>{`Blog – ${config.siteTitle}`}</title>
        </Helmet>
      )}

      <Container>
        <TextContainer>
          <h1>{title}</h1>
          <h2>{subtitle}</h2>
        </TextContainer>
        {body && <PageBody body={body} />}
      </Container>

      {/*<GreenLine />*/}

      <Container>
        {/*
        <TextContainer>
          <h1>{title}</h1>
          <h2>{subtitle}</h2>
        </TextContainer>
        */}

        <StyledDiv>
          {/*
          <PageBody body={body} />
          */}

          <CardList relaxed>
            {posts.map(({ node: post }) => (
              <Card key={post.id} {...post} />
            ))}
          </CardList>
        </StyledDiv>

        <Pagination context={pageContext} />
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query($skip: Int!, $limit: Int!) {
    contentfulPage(slug: { eq: "blog" }) {
      id
      title
      subtitle
      headerImage {
        id
        fluid(maxWidth: 1280) {
          srcWebp
          srcSetWebp
          srcSet
          src
        }
      }
      body {
        json
      }
    }
    allContentfulPost(
      sort: { fields: [publishDate], order: DESC }
      limit: $limit
      skip: $skip
      filter: { node_locale: { eq: "de-CH" } }
    ) {
      edges {
        node {
          title
          id
          slug
          tags {
            title
            slug
          }
          publishDate(formatString: "DD.MM.YYYY")
          heroImage {
            title
            fluid(quality: 100, resizingBehavior: NO_CHANGE, toFormat: NO_CHANGE) {
              srcWebp
              srcSetWebp
              srcSet
              src
              sizes
              aspectRatio
              ...GatsbyContentfulFluid_withWebp_noBase64
            }
          }
          auszug
          body {
            json
          }
        }
      }
    }
  }
`
export default Blog
