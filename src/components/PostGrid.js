import React, { useState } from 'react'
import { graphql, StaticQuery } from 'gatsby'
import styled from 'styled-components'
import StackGrid from 'react-stack-grid'
import Img from 'gatsby-image'
import { Dialog } from '@reach/dialog'
import PageTitle from './PageTitle'
import '@reach/dialog/styles.css'

const PostGalleryGrid = props => {
  const [showLightbox, setShowLightbox] = useState(false)
  const [selectedImage, setSelectedImage] = useState(null)

  const handlePrevRequest = (i, length) => {
    setSelectedImage((i - 1 + length) % length)
  }
  const handleNextRequest = (i, length) => {
    setSelectedImage((i + 1) % length)
  }

  const ImageContainer = styled.div`
    &:hover {
      cursor: zoom-in;
    }
  `

  const PrevButton = styled.button`
    top: calc(50% - 18px);
    position: absolute;
    height: 100%;
    min-width: 30px;
    top: 0;
    i {
      top: calc(50% - 18px);
      margin-left: -38px !important;
      @media screen and (min-width: 500px) {
        margin-left: 0 !important;
      }
    }
    &:hover {
      cursor: pointer;
      i {
        @media screen and (min-width: 500px) {
          -webkit-animation: slide-left 0.5s
            cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
          animation: slide-left 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
        }
      }
    }
  `

  const NextButton = styled.button`
    right: 0;
    position: absolute;
    top: 0;
    height: 100%;
    min-width: 35px;
    i {
      top: calc(50% - 18px);
      right: 0;
      margin-right: -35px !important;
      @media screen and (min-width: 500px) {
        margin-right: 0 !important;
      }
    }
    &:hover {
      cursor: pointer;
      i {
        @media screen and (min-width: 500px) {
          -webkit-animation: slide-right 0.5s
            cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
          animation: slide-right 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
        }
      }
    }
  `

  const CloseButton = styled.button`
    position: absolute;
    background: white;
    top: -13px;
    right: -10px;
    z-index: 999;
    &:hover {
      cursor: pointer;
      i {
        -webkit-animation: heartbeat 1.5s ease-in-out infinite both;
        animation: heartbeat 1.5s ease-in-out infinite both;
      }
    }
  `

  const Wrapper = styled.section`
    position: relative;
    margin: 1rem;
    z-index: 999;
  `

  const imgArray = props.items.map(el => el.contentful_id)
  // console.log(imgArray)

  /* No static query with variables allowed
  contentful_id: { in: $imgArray }
  */

  return (
    <StaticQuery
      query={graphql`
        query allImgQuery {
          source: allContentfulAsset(filter: { node_locale: { eq: "de-CH" } }) {
            edges {
              node {
                id
                contentful_id
                fluid(maxWidth: 1280) {
                  ...GatsbyContentfulFluid_withWebp_noBase64
                  srcSet
                  src
                }
              }
            }
          }
        }
      `}
      render={data => {
        const images = data.source.edges.filter(el =>
          imgArray.includes(el.node.contentful_id)
        )
        return (
          <Wrapper>
            <StackGrid
              duration={0}
              columnWidth={props.columnWidth}
              gutterWidth={5}
              gutterHeight={5}
            >
              {images.map((item, i) => (
                <ImageContainer
                  key={`project-image-${i}`}
                  onClick={() => {
                    setShowLightbox(true)
                    setSelectedImage(i)
                  }}
                >
                  <Img
                    alt={item.node.title}
                    key={`image-${i}`}
                    fluid={item.node.fluid}
                    src={item.node.fluid.src}
                  />
                </ImageContainer>
              ))}
            </StackGrid>
            {showLightbox && (
              <Dialog
                onDismiss={() => setShowLightbox(false)}
                // onClick={() => setShowLightbox(false)}
                style={{
                  position: 'relative',
                  width: '75vw',
                  padding: '.5rem',
                }}
              >
                <Img fluid={images[selectedImage].node.fluid} />
                <PrevButton
                  onClick={() =>
                    handlePrevRequest(selectedImage, images.length)
                  }
                >
                  <i className="icon big inverted long arrow alternate left" />
                </PrevButton>
                <NextButton
                  onClick={() =>
                    handleNextRequest(selectedImage, images.length)
                  }
                >
                  <i className="icon big inverted long arrow alternate right" />
                </NextButton>
                <CloseButton
                  className="ui icon circular grey button"
                  onClick={() => setShowLightbox(false)}
                >
                  <i className="ui icon remove" />
                </CloseButton>
              </Dialog>
            )}
          </Wrapper>
        )
      }}
    />
  )
}

export default PostGalleryGrid
