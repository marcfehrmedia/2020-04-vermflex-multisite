import React from 'react'
import styled from 'styled-components'
import { BLOCKS, MARKS, INLINES } from '@contentful/rich-text-types'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'

require('prismjs/themes/prism.css')

const Body = styled.p`
  position: relative;
  width: 100%;
  line-height: 1.5;
  float: left;
  display: flex;
  padding-top: 2rem;
`

const TestimonialBody = props => {
  const content = props.body.json

  const options = {
    renderMark: {
      [INLINES.HYPERLINK]: link => (
        <a target={link.target} rel="noopener noreferrer" href={link.href}>
          {content}
        </a>
      ),
      [MARKS.BOLD]: text => <strong>{text}</strong>,
      [BLOCKS.HEADING_1]: text => <h1>{text}</h1>,
      [BLOCKS.HEADING_2]: text => <h2>{text}</h2>,
      [BLOCKS.HEADING_3]: text => <h3>{text}</h3>,
      [BLOCKS.HEADING_4]: text => <h4>{text}</h4>,
      [BLOCKS.HEADING_5]: text => <h5>{text}</h5>,
      [BLOCKS.HEADING_6]: text => <h6>{text}</h6>,
      [BLOCKS.LIST_ITEM]: text => <h4>{text}</h4>,
      [BLOCKS.UL_LIST]: text => <h4>{text}</h4>,
      [BLOCKS.OL_LIST]: text => <ol>{text}</ol>,
      [BLOCKS.PARAGRAPH]: text => <p>{text}</p>,
    },
    renderNode: {
      [BLOCKS.PARAGRAPH]: (node, children) => <p>{children}</p>,
    },
  }

  return <Body>{documentToReactComponents(props.body.json, options)}</Body>
}

export default TestimonialBody
