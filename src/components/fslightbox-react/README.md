# React Fullscreen Lightbox Pro
Fullscreen Lightbox is plugin for displaying images and videos in clean overlaying box.

Website: https://fslightbox.com


| Dependency | Version |
| --- | --- |
| react | at least 16.8.0 |
| react-dom | at least 16.8.0 |
| prop-types | at least 15.6.2 |

## Basic usage

### Installation
Download .tgz archive from download page. Put it somewhere in your project for example: ./src/lib
Then run npm install command with path to that archive. You need to be in directory where your project package.json file is.
``` 
$ npm install ./src/lib/[lightbox archive name] 
```
Example:
``` 
$ npm install ./src/lib/fslightbox-react-pro-1.0.0.tgz
```

### Example

```jsx
import React, { useState } from 'react';
import FsLightbox from 'fslightbox-react';

function App() {
    // initial toggler value determines if lightbox should be opened on init
    // if toggler is updated when lightbox is closed it will open it
    // if toggler is updated when lightbox is opened it will close it
    const [toggler, setToggler] = useState(false);

    return (
        <>
            <button onClick={() => setToggler(!toggler)}>
                Toggle Lightbox
            </button>
            <FsLightbox
                toggler={toggler}
                sources={[
                    'https://i.imgur.com/fsyrScY.jpg',
                    'https://www.youtube.com/watch?v=xshEZzpS4CQ',
                    'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
                ]}
            />
        </>
    );
}

export default App;

```

## Demo
Available at: https://fslightbox.com/react

## Browser Compatibility

| Browser | Works? |
| --- | --- |
| Chrome | Yes |
| Firefox | Yes |
| Safari | Yes |
| Edge | Yes |
| IE 11 | Yes |

