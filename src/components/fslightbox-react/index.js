module.exports = function(e) {
	var t = {};

	function n(r) {
		if (t[r]) return t[r].exports;
		var o = t[r] = {
			i: r,
			l: !1,
			exports: {}
		};
		return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports
	}
	return n.m = e, n.c = t, n.d = function(e, t, r) {
		n.o(e, t) || Object.defineProperty(e, t, {
			enumerable: !0,
			get: r
		})
	}, n.r = function(e) {
		"undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
			value: "Module"
		}), Object.defineProperty(e, "__esModule", {
			value: !0
		})
	}, n.t = function(e, t) {
		if (1 & t && (e = n(e)), 8 & t) return e;
		if (4 & t && "object" == typeof e && e && e.__esModule) return e;
		var r = Object.create(null);
		if (n.r(r), Object.defineProperty(r, "default", {
			enumerable: !0,
			value: e
		}), 2 & t && "string" != typeof e)
			for (var o in e) n.d(r, o, function(t) {
				return e[t]
			}.bind(null, o));
		return r
	}, n.n = function(e) {
		var t = e && e.__esModule ? function() {
			return e.default
		} : function() {
			return e
		};
		return n.d(t, "a", t), t
	}, n.o = function(e, t) {
		return Object.prototype.hasOwnProperty.call(e, t)
	}, n.p = "", n(n.s = 2)
}([function(e, t) {
	e.exports = require("react")
}, function(e, t) {
	e.exports = require("prop-types")
}, function(e, t, n) {
	"use strict";
	n.r(t);
	var r = "fslightbox-";

	function o() {
		var e = document.createElement("style");
		e.className = "fslightbox-styles", e.appendChild(document.createTextNode(".fslightbox-fade-in{animation:fslightbox-fade-in .3s cubic-bezier(0,0,.7,1)}.fslightbox-fade-out{animation:fslightbox-fade-out .3s ease}.fslightbox-fade-in-strong{animation:fslightbox-fade-in-strong .3s cubic-bezier(0,0,.7,1)}.fslightbox-fade-out-strong{animation:fslightbox-fade-out-strong .3s ease}@keyframes fslightbox-fade-in{from{opacity:.65}to{opacity:1}}@keyframes fslightbox-fade-out{from{opacity:.35}to{opacity:0}}@keyframes fslightbox-fade-in-strong{from{opacity:.3}to{opacity:1}}@keyframes fslightbox-fade-out-strong{from{opacity:1}to{opacity:0}}.fslightbox-scale-in{animation:fslightbox-scale-in .5s ease}@keyframes fslightbox-scale-in{from{opacity:0;transform:scale(.5)}to{opacity:1;transform:scale(1)}}.fslightbox-absoluted{position:absolute;top:0;left:0}.fslightbox-cursor-grabbing{cursor:grabbing}.fslightbox-full-dimension{width:100%;height:100%}.fslightbox-open{overflow:hidden;height:100%}.fslightbox-flex-centered{display:flex;justify-content:center;align-items:center}.fslightbox-opacity-0{opacity:0!important}.fslightbox-opacity-1{opacity:1!important}.fslightbox-scrollbarfix{padding-right:17px}.fslightbox-transform-transition{transition:transform .3s}.fslightbox-container{font-family:Helvetica,sans-serif;position:fixed;top:0;left:0;background:linear-gradient(rgba(30,30,30,.9),#000 1810%);z-index:9999999;touch-action:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-tap-highlight-color:transparent}.fslightbox-container *{box-sizing:border-box}.fslightbox-svg-path{transition:fill .15s ease;fill:#d1d2d2}.fslightbox-loader{display:block;margin:auto;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);width:67px;height:67px}.fslightbox-loader div{box-sizing:border-box;display:block;position:absolute;width:54px;height:54px;margin:6px;border:5px solid;border-color:#999 transparent transparent transparent;border-radius:50%;animation:fslightbox-loader 1.2s cubic-bezier(.5,0,.5,1) infinite}.fslightbox-loader div:nth-child(1){animation-delay:-.45s}.fslightbox-loader div:nth-child(2){animation-delay:-.3s}.fslightbox-loader div:nth-child(3){animation-delay:-.15s}@keyframes fslightbox-loader{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.fslightbox-thumbs-loader{width:54px!important;height:54px!important}.fslightbox-thumbs-loader div{border-width:4px!important;width:44px!important;height:44px!important}.fslightbox-sources-outers-wrapper{z-index:2;top:0;transition:transform .3s}.fslightbox-nav{height:45px;width:100%;transition:opacity .3s}.fslightbox-slide-number-container{display:flex;justify-content:center;align-items:center;position:relative;height:100%;font-size:15px;color:#d7d7d7;z-index:0;max-width:55px;text-align:left}.fslightbox-slide-number-container .fslightbox-flex-centered{height:100%}.fslightbox-slash{display:block;margin:0 5px;width:1px;height:12px;transform:rotate(15deg);background:#fff}.fslightbox-toolbar{position:absolute;z-index:3;right:0;top:0;height:100%;display:flex;background:rgba(35,35,35,.65)}.fslightbox-toolbar-button{height:100%;width:45px;cursor:pointer}.fslightbox-toolbar-button:hover .fslightbox-svg-path{fill:#fff}.fslightbox-slide-btn-container{display:flex;align-items:center;padding:12px 12px 12px 6px;position:absolute;top:50%;cursor:pointer;z-index:3;transform:translateY(-50%);transition:opacity .3s}@media (min-width:476px){.fslightbox-slide-btn-container{padding:22px 22px 22px 6px}}@media (min-width:768px){.fslightbox-slide-btn-container{padding:30px 30px 30px 6px}}.fslightbox-slide-btn-container:hover .fslightbox-svg-path{fill:#f1f1f1}.fslightbox-slide-btn{padding:9px;font-size:26px;background:rgba(35,35,35,.65)}@media (min-width:768px){.fslightbox-slide-btn{padding:10px}}@media (min-width:1600px){.fslightbox-slide-btn{padding:11px}}.fslightbox-slide-btn-container-previous{left:0}@media (max-width:475.99px){.fslightbox-slide-btn-container-previous{padding-left:3px!important}}.fslightbox-slide-btn-container-next{right:0;padding-left:12px!important;padding-right:3px!important}@media (min-width:476px){.fslightbox-slide-btn-container-next{padding-left:22px!important}}@media (min-width:768px){.fslightbox-slide-btn-container-next{padding-left:30px!important}}@media (min-width:476px){.fslightbox-slide-btn-container-next{padding-right:6px!important}}.fslightbox-down-event-detector{position:absolute;z-index:1}.fslightbox-slide-pointering-hoverer{z-index:4}.fslightbox-slideshow-bar{width:0;height:2px;z-index:4;opacity:0;background:#fff;transition:opacity .4s}.fslightbox-invalid-file-wrapper{font-size:26px;color:#eaebeb;margin:auto}.fslightbox-video{object-fit:cover}.fslightbox-youtube-iframe{border:0}.fslightbox-source{position:relative;margin:auto;z-index:2;opacity:0;cursor:zoom-in;transform:translateZ(0);transition:transform .3s;backface-visibility:hidden}.fslightbox-source-outer{will-change:transform}.fslightbox-source-inner{transition:transform .3s}.fslightbox-source-inner-pinching{transition:transform .1s linear}.fslightbox-thumbs{position:absolute;bottom:0;left:0;width:100%;z-index:-1;background:linear-gradient(180deg,rgba(0,0,0,0),#1e1e1e 100%);opacity:0;transition:opacity .2s;backface-visibility:hidden;padding:0 5px 12px 5px;height:114px}@media (min-width:992px){.fslightbox-thumbs{padding-bottom:13px;height:120px}}@media (min-width:1600px){.fslightbox-thumbs{padding-bottom:14px;height:126px}}.fslightbox-thumbs-active{opacity:1;z-index:3}.fslightbox-thumbs-inner{height:100%;display:inline-flex;justify-content:flex-start;align-items:center;will-change:transform}.fslightbox-thumb-wrapper{position:relative;height:100%;margin:0 4px;opacity:0;transition:opacity .3s}.fslightbox-thumb-wrapper svg{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);cursor:pointer;z-index:1}.fslightbox-thumb-wrapper path{fill:#fff}.fslightbox-thumb-wrapper-darkener{position:absolute;top:2px;left:2px;width:calc(100% - 4px);height:calc(100% - 4px);background:rgba(0,0,0,.4);cursor:pointer}.fslightbox-thumb{cursor:pointer;border-radius:1px;height:100%;width:auto!important;border:2px solid transparent}.fslightbox-thumb-active{border:2px solid #fff!important}.fslightbox-thumb-invalid{background:linear-gradient(to bottom,#0f0f0f,rgba(15,15,15,.5));display:inline-block;min-width:155px}.fslightbox-thumbs-cursorer{z-index:4;cursor:grabbing}.fslightbox-caption{position:absolute;bottom:0;left:50%;width:100%;background:linear-gradient(180deg,rgba(0,0,0,0),#1e1e1e 100%);transform:translateX(-50%);opacity:0;transition:opacity .2s;z-index:-1}.fslightbox-caption-inner{padding:25px;max-width:1200px;color:#eee;text-align:center;font-size:14px}@media (min-width:768px){.fslightbox-caption-inner{padding:30px 25px}}.fslightbox-caption-active{opacity:1;z-index:3}")), document.head.appendChild(e)
	}

	function i(e) {
		return (i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
			return typeof e
		} : function(e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
		})(e)
	}
	"object" === ("undefined" == typeof document ? "undefined" : i(document)) && o();
	var s = n(0),
		a = n.n(s),
		u = n(1),
		c = n.n(u);

	function l(e) {
		return {
			onClick: e.core.clickZoomer.zoomIn,
			viewBox: "0 0 30 30",
			width: "20px",
			height: "20px",
			d: "M 13 3 C 7.4889971 3 3 7.4889971 3 13 C 3 18.511003 7.4889971 23 13 23 C 15.396508 23 17.597385 22.148986 19.322266 20.736328 L 25.292969 26.707031 A 1.0001 1.0001 0 1 0 26.707031 25.292969 L 20.736328 19.322266 C 22.148986 17.597385 23 15.396508 23 13 C 23 7.4889971 18.511003 3 13 3 z M 13 5 C 17.430123 5 21 8.5698774 21 13 C 21 17.430123 17.430123 21 13 21 C 8.5698774 21 5 17.430123 5 13 C 5 8.5698774 8.5698774 5 13 5 z M 12.984375 7.9863281 A 1.0001 1.0001 0 0 0 12 9 L 12 12 L 9 12 A 1.0001 1.0001 0 1 0 9 14 L 12 14 L 12 17 A 1.0001 1.0001 0 1 0 14 17 L 14 14 L 17 14 A 1.0001 1.0001 0 1 0 17 12 L 14 12 L 14 9 A 1.0001 1.0001 0 0 0 12.984375 7.9863281 z",
			title: "Zoom In"
		}
	}

	function f(e) {
		return {
			onClick: e.core.clickZoomer.zoomOut,
			viewBox: "0 0 30 30",
			width: "20px",
			height: "20px",
			d: "M 13 3 C 7.4889971 3 3 7.4889971 3 13 C 3 18.511003 7.4889971 23 13 23 C 15.396508 23 17.597385 22.148986 19.322266 20.736328 L 25.292969 26.707031 A 1.0001 1.0001 0 1 0 26.707031 25.292969 L 20.736328 19.322266 C 22.148986 17.597385 23 15.396508 23 13 C 23 7.4889971 18.511003 3 13 3 z M 13 5 C 17.430123 5 21 8.5698774 21 13 C 21 17.430123 17.430123 21 13 21 C 8.5698774 21 5 17.430123 5 13 C 5 8.5698774 8.5698774 5 13 5 z M 9 12 A 1.0001 1.0001 0 1 0 9 14 L 17 14 A 1.0001 1.0001 0 1 0 17 12 L 9 12 z",
			title: "Zoom Out"
		}
	}

	function d(e) {
		return {
			onClick: e.core.thumbsToggler.toggleThumbs,
			viewBox: "0 0 22 22",
			width: "17px",
			height: "17px",
			d: "M 3 2 C 2.448 2 2 2.448 2 3 L 2 6 C 2 6.552 2.448 7 3 7 L 6 7 C 6.552 7 7 6.552 7 6 L 7 3 C 7 2.448 6.552 2 6 2 L 3 2 z M 10 2 C 9.448 2 9 2.448 9 3 L 9 6 C 9 6.552 9.448 7 10 7 L 13 7 C 13.552 7 14 6.552 14 6 L 14 3 C 14 2.448 13.552 2 13 2 L 10 2 z M 17 2 C 16.448 2 16 2.448 16 3 L 16 6 C 16 6.552 16.448 7 17 7 L 20 7 C 20.552 7 21 6.552 21 6 L 21 3 C 21 2.448 20.552 2 20 2 L 17 2 z M 3 9 C 2.448 9 2 9.448 2 10 L 2 13 C 2 13.552 2.448 14 3 14 L 6 14 C 6.552 14 7 13.552 7 13 L 7 10 C 7 9.448 6.552 9 6 9 L 3 9 z M 10 9 C 9.448 9 9 9.448 9 10 L 9 13 C 9 13.552 9.448 14 10 14 L 13 14 C 13.552 14 14 13.552 14 13 L 14 10 C 14 9.448 13.552 9 13 9 L 10 9 z M 17 9 C 16.448 9 16 9.448 16 10 L 16 13 C 16 13.552 16.448 14 17 14 L 20 14 C 20.552 14 21 13.552 21 13 L 21 10 C 21 9.448 20.552 9 20 9 L 17 9 z M 3 16 C 2.448 16 2 16.448 2 17 L 2 20 C 2 20.552 2.448 21 3 21 L 6 21 C 6.552 21 7 20.552 7 20 L 7 17 C 7 16.448 6.552 16 6 16 L 3 16 z M 10 16 C 9.448 16 9 16.448 9 17 L 9 20 C 9 20.552 9.448 21 10 21 L 13 21 C 13.552 21 14 20.552 14 20 L 14 17 C 14 16.448 13.552 16 13 16 L 10 16 z M 17 16 C 16.448 16 16 16.448 16 17 L 16 20 C 16 20.552 16.448 21 17 21 L 20 21 C 20.552 21 21 20.552 21 20 L 21 17 C 21 16.448 20.552 16 20 16 L 17 16 z",
			title: "Thumbnails"
		}
	}

	function h(e) {
		return {
			onClick: e.core.lightboxCloser.close,
			viewBox: "0 0 24 24",
			width: "20px",
			height: "20px",
			d: "M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z",
			title: "Close"
		}
	}

	function m(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return p(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return p(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function p(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}

	function g(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return b(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return b(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function b(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var v, x = function(e) {
		var t = e.fsLightbox,
			n = t.componentsServices.toolbarButtons.fullscreen,
			r = t.core,
			o = r.fullscreenToggler,
			i = o.exitFullscreen,
			a = o.enterFullscreen,
			u = r.buttonsBuildersFacade.getTwoStatesToolbarButton,
			c = !1;
		"undefined" != typeof window && (c = !!document.fullscreenElement);
		var l = g(Object(s.useState)(c), 2),
			f = l[0],
			d = l[1];
		return n.get = function() {
			return f
		}, n.set = d, u("fullscreen", {
			enter: {
				onClick: a,
				viewBox: "0 0 18 18",
				width: "20px",
				height: "20px",
				d: "M4.5 11H3v4h4v-1.5H4.5V11zM3 7h1.5V4.5H7V3H3v4zm10.5 6.5H11V15h4v-4h-1.5v2.5zM11 3v1.5h2.5V7H15V3h-4z",
				title: "Enter fullscreen"
			},
			exit: {
				onClick: i,
				viewBox: "0 0 950 1024",
				width: "24px",
				height: "24px",
				d: "M682 342h128v84h-212v-212h84v128zM598 810v-212h212v84h-128v128h-84zM342 342v-128h84v212h-212v-84h128zM214 682v-84h212v212h-84v-128h-128z",
				title: "Exit fullscreen"
			}
		})
	};

	function y(e, t, n) {
		return t in e ? Object.defineProperty(e, t, {
			value: n,
			enumerable: !0,
			configurable: !0,
			writable: !0
		}) : e[t] = n, e
	}
	var S = (y(v = {}, "slideshow", (function(e) {
			var t, n = e.fsLightbox,
				r = n.componentsServices.toolbarButtons.slideshow,
				o = n.core,
				i = o.buttonsBuildersFacade,
				a = o.slideshowManager,
				u = a.turnOnSlideshow,
				c = a.turnOffSlideshow,
				l = m(Object(s.useState)(!1), 2),
				f = l[0],
				d = l[1];
			return r.get = function() {
				return f
			}, r.set = d, (t = r).onUpdate && (t.onUpdate(), delete t.onUpdate), i.getTwoStatesToolbarButton("slideshow", {
				start: {
					onClick: u,
					viewBox: "0 0 30 30",
					width: "16px",
					height: "16px",
					d: "M 6 3 A 1 1 0 0 0 5 4 A 1 1 0 0 0 5 4.0039062 L 5 15 L 5 25.996094 A 1 1 0 0 0 5 26 A 1 1 0 0 0 6 27 A 1 1 0 0 0 6.5800781 26.8125 L 6.5820312 26.814453 L 26.416016 15.908203 A 1 1 0 0 0 27 15 A 1 1 0 0 0 26.388672 14.078125 L 6.5820312 3.1855469 L 6.5800781 3.1855469 A 1 1 0 0 0 6 3 z",
					title: "Turn on slideshow"
				},
				pause: {
					onClick: c,
					viewBox: "0 0 356.19 356.19",
					width: "14px",
					height: "14px",
					d: "M121,0c18,0,33,15,33,33v372c0,18-15,33-33,33s-32-15-32-33V33C89,15,103,0,121,0zM317,0c18,0,32,15,32,33v372c0,18-14,33-32,33s-33-15-33-33V33C284,15,299,0,317,0z",
					title: "Turn off slideshow"
				}
			})
		})), y(v, "fullscreen", x), v),
		w = function(e) {
			var t = e.viewBox,
				n = e.width,
				o = e.height,
				i = e.d;
			return a.a.createElement("svg", {
				width: n,
				height: o,
				viewBox: t,
				xmlns: "http://www.w3.org/2000/svg"
			}, a.a.createElement("path", {
				className: r + "svg-path",
				d: i
			}))
		},
		T = function(e) {
			var t = e.onClick,
				n = e.viewBox,
				r = e.width,
				o = e.height,
				i = e.d,
				s = e.title;
			return a.a.createElement("div", {
				onClick: t,
				className: "fslightbox-toolbar-button fslightbox-flex-centered",
				title: s
			}, a.a.createElement(w, {
				viewBox: n,
				width: r,
				height: o,
				d: i
			}))
		},
		C = function(e) {
			var t = e.fsLightbox,
				n = t.core.buttonsBuildersFacade.getOneStateToolbarButton,
				o = t.props,
				i = o.customToolbarButtons,
				s = o.disableThumbs,
				u = [];

			function c(e, r) {
				u.push(n(e, r(t)))
			}

			function m(e) {
				var n = S[e];
				u.push(a.a.createElement(n, {
					fsLightbox: t,
					key: e
				}))
			}
			if (s || c("thumbs", d), c("zoomIn", l), c("zoomOut", f), m("slideshow"), m("fullscreen"), c("close", h), i)
				for (var p = 0; p < i.length; p++) {
					var g = i[p];
					u.unshift(a.a.createElement(T, {
						onClick: g.onClick,
						viewBox: g.viewBox,
						width: g.width,
						height: g.height,
						d: g.d,
						title: g.title,
						key: g.title
					}))
				}
			return a.a.createElement("div", {
				className: r + "toolbar"
			}, u)
		};

	function L(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return A(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return A(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function A(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var I = function(e) {
			var t = e.fsLightbox,
				n = t.componentsServices,
				o = t.data.sources,
				i = t.stageIndexes,
				u = L(Object(s.useState)(i.current + 1), 2),
				c = u[0],
				l = u[1];
			n.setSlideNumber = function(e) {
				l(e)
			};
			var f = a.a.createRef(),
				d = a.a.createRef();
			return Object(s.useEffect)((function() {
				d.current.offsetWidth > 55 && (f.current.style.justifyContent = "flex-start")
			}), []), a.a.createElement("div", {
				ref: f,
				className: r + "slide-number-container"
			}, a.a.createElement("div", {
				ref: d,
				className: "fslightbox-flex-centered"
			}, a.a.createElement("span", null, c), a.a.createElement("span", {
				className: r + "slash"
			}), a.a.createElement("span", null, o.length)))
		},
		O = function(e) {
			var t = e.fsLightbox;
			return a.a.createElement("div", {
				ref: t.elements.nav,
				className: r + "nav fslightbox-absoluted"
			}, a.a.createElement(C, {
				fsLightbox: t
			}), t.data.sources.length > 1 && a.a.createElement(I, {
				fsLightbox: t
			}))
		},
		E = function(e) {
			var t = e.fsLightbox.core;
			return (0, t.buttonsBuildersFacade.getOneStateSlideButton)("previous", {
				onClick: t.slideChangeFacade.changeToPrevious,
				d: "M18.271,9.212H3.615l4.184-4.184c0.306-0.306,0.306-0.801,0-1.107c-0.306-0.306-0.801-0.306-1.107,0L1.21,9.403C1.194,9.417,1.174,9.421,1.158,9.437c-0.181,0.181-0.242,0.425-0.209,0.66c0.005,0.038,0.012,0.071,0.022,0.109c0.028,0.098,0.075,0.188,0.142,0.271c0.021,0.026,0.021,0.061,0.045,0.085c0.015,0.016,0.034,0.02,0.05,0.033l5.484,5.483c0.306,0.307,0.801,0.307,1.107,0c0.306-0.305,0.306-0.801,0-1.105l-4.184-4.185h14.656c0.436,0,0.788-0.353,0.788-0.788S18.707,9.212,18.271,9.212z",
				title: "Previous slide"
			})
		},
		z = function(e) {
			var t = e.fsLightbox.core;
			return (0, t.buttonsBuildersFacade.getOneStateSlideButton)("next", {
				onClick: t.slideChangeFacade.changeToNext,
				d: "M1.729,9.212h14.656l-4.184-4.184c-0.307-0.306-0.307-0.801,0-1.107c0.305-0.306,0.801-0.306,1.106,0l5.481,5.482c0.018,0.014,0.037,0.019,0.053,0.034c0.181,0.181,0.242,0.425,0.209,0.66c-0.004,0.038-0.012,0.071-0.021,0.109c-0.028,0.098-0.075,0.188-0.143,0.271c-0.021,0.026-0.021,0.061-0.045,0.085c-0.015,0.016-0.034,0.02-0.051,0.033l-5.483,5.483c-0.306,0.307-0.802,0.307-1.106,0c-0.307-0.305-0.307-0.801,0-1.105l4.184-4.185H1.729c-0.436,0-0.788-0.353-0.788-0.788S1.293,9.212,1.729,9.212z",
				title: "Next slide"
			})
		},
		F = function(e) {
			var t = e.additionalClassName,
				n = (e.testId, r + "loader");
			return t && (n += " " + t), a.a.createElement("div", {
				className: n
			}, a.a.createElement("div", null), a.a.createElement("div", null), a.a.createElement("div", null), a.a.createElement("div", null))
		};

	function P(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return k(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return k(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function k(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var B = function(e) {
		var t = e.fsLightbox,
			n = t.componentsServices.updateSourceInnerCollection,
			r = t.core.stageManager.isSourceInStage,
			o = t.elements,
			i = o.sourcesComponents,
			u = o.sourcesInners,
			c = t.props.loadOnlyCurrentSource,
			l = t.stageIndexes.current,
			f = e.i,
			d = P(Object(s.useState)(!1), 2),
			h = d[0],
			m = d[1];
		return n[f] = function() {
			m(!h)
		}, a.a.createElement("div", {
			ref: u[f],
			className: "fslightbox-source-inner"
		}, f === l || !c && r(f) ? i[f] : null)
	};

	function M(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return N(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return N(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function N(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var j = function(e) {
		var t = e.fsLightbox,
			n = e.i,
			r = t.componentsServices.isSourceLoadedCollection,
			o = t.elements.sourcesOuters,
			i = M(Object(s.useState)(!1), 2),
			u = i[0],
			c = i[1];
		return r[n] = {
			get: function() {
				return u
			},
			set: c
		}, a.a.createElement("div", {
			ref: o[n],
			className: "fslightbox-source-outer fslightbox-absoluted fslightbox-full-dimension fslightbox-flex-centered"
		}, !u && a.a.createElement(F, null), a.a.createElement(B, {
			fsLightbox: t,
			i: n
		}))
	};

	function U() {
		try {
			return document.createEvent("TouchEvent"), !0
		} catch (e) {
			return !1
		}
	}

	function H(e, t) {
		return function() {
			t.apply(void 0, arguments) && e.apply(void 0, arguments)
		}
	}

	function R(e) {
		return !e.touches || e.touches.length <= 2
	}

	function Y() {
		this.getMouseDownListenerFunc = function(e) {
			return U() ? void 0 : H(e, R)
		}, this.getTouchStartListenerForFunc = function(e) {
			return U() ? H(e, R) : void 0
		}
	}
	var D = function(e) {
		for (var t = e.fsLightbox, n = t.data.sources, r = t.elements.sourcesOutersWrapper, o = t.core.sourcesPointerDown.listener, i = [], s = new Y, u = 0; u < n.length; u++) i.push(a.a.createElement(j, {
			fsLightbox: t,
			i: u,
			key: u
		}));
		return a.a.createElement("div", {
			className: "fslightbox-sources-outers-wrapper fslightbox-absoluted fslightbox-full-dimension",
			ref: r,
			onMouseDown: s.getMouseDownListenerFunc(o),
			onTouchStart: s.getTouchStartListenerForFunc(o)
		}, i)
	};

	function W(e) {
		for (var t = e.data.sources, n = [], r = 0; r < t.length; r++) n.push(a.a.createRef());
		return n
	}

	function X(e, t) {
		for (var n in t) e[n] = t[n]
	}

	function Z(e) {
		var t, n = e.props.disableLocalStorage,
			r = 0,
			o = {};
		this.getSourceTypeFromLocalStorageByUrl = function(e) {
			return t[e] ? t[e] : i(e)
		}, this.handleReceivedSourceTypeForUrl = function(e, t) {
			void 0 !== o[t] && (r--, o[t] = e, s())
		};
		var i = function(e) {
				r++, o[e] = !1
			},
			s = function() {
				0 === r && (X(t, o), localStorage.setItem("fslightbox-types", JSON.stringify(t)))
			};
		n ? (this.getSourceTypeFromLocalStorageByUrl = function() {}, this.handleReceivedSourceTypeForUrl = function() {}) : (t = JSON.parse(localStorage.getItem("fslightbox-types"))) || (t = {}, this.getSourceTypeFromLocalStorageByUrl = i)
	}
	var V = function(e) {
			var t = e.fsLightbox,
				n = t.data.sources,
				r = t.elements.sources,
				o = t.collections.sourcesLoadsHandlers,
				i = e.i;
			return a.a.createElement("img", {
				onLoad: o[i].handleImageLoad,
				className: "fslightbox-source",
				ref: r[i],
				src: n[i],
				alt: n[i]
			})
		},
		q = function(e) {
			var t = e.fsLightbox,
				n = t.collections.sourcesLoadsHandlers,
				o = t.elements.sources,
				i = t.data.sources,
				s = t.props.videosPosters,
				u = e.i;
			return setTimeout(n[u].handleNotMetaDatedVideoLoad, 3e3), a.a.createElement("video", {
				onLoadedMetadata: n[u].handleVideoLoad,
				className: "fslightbox-source " + r + "video",
				controls: !0,
				ref: o[u],
				poster: s && s[u]
			}, a.a.createElement("source", {
				src: i[u]
			}))
		};
	var _ = function(e) {
			var t, n = e.fsLightbox,
				o = n.data.sources,
				i = n.elements.sources,
				u = n.collections.sourcesLoadsHandlers,
				c = e.i;
			return Object(s.useEffect)(u[c].handleYoutubeLoad), a.a.createElement("iframe", {
				className: "fslightbox-source " + r + "youtube-iframe",
				ref: i[c],
				src: "https://www.youtube.com/embed/" + (t = o[c], t.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/)[2] + "?enablejsapi=1"),
				allowFullScreen: !0
			})
		},
		$ = function(e) {
			var t = e.fsLightbox,
				n = t.data.sources,
				r = t.elements.sources,
				o = t.collections.sourcesLoadsHandlers,
				i = e.i;
			Object(s.useEffect)(o[i].handleCustomLoad);
			var u = n[i].props.className;
			return a.a.cloneElement(n[i], {
				ref: r[i],
				className: u ? u + " fslightbox-source" : "fslightbox-source"
			})
		},
		G = function(e) {
			var t = e.fsLightbox,
				n = t.componentsServices.isSourceLoadedCollection,
				o = t.data.initialAnimation,
				i = t.elements.sourcesInners,
				u = e.i;
			return Object(s.useEffect)((function() {
				n[u].set(!0), i[u].current.classList.add(o)
			})), a.a.createElement("div", {
				className: r + "invalid-file-wrapper fslightbox-flex-centered"
			}, "Invalid source")
		};

	function J(e, t, n, r) {
		var o = this,
			i = e.collections.sourcesTransformers,
			s = e.data.sourcesTranslatesY,
			a = e.elements.sources,
			u = 1;
		this.styleSourceUsingScaleAndHeight = function(e, c) {
			innerWidth < innerHeight && n > r + c ? (u = 1 / e, s[t] = a[t].current.getBoundingClientRect().height * (u - 1) / 2, i[t].translateYAndScale(s[t], u)) : (o.ifSourcesScaledResetScale(), delete s[t])
		}, this.ifSourcesScaledResetScale = function() {
			1 !== u && (u = 1, i[t].translateYAndScale(0, 1), delete s[t])
		}
	}

	function K(e, t, n, r) {
		var o = this,
			i = e.data,
			s = e.elements,
			a = s.sources,
			u = s.thumbsContainer,
			c = s.captions,
			l = e.resolve,
			f = i.captionedSourcesOutersScales,
			d = l(J, [t, n, r]),
			h = n / r,
			m = 0;
		this.styleAll = function() {
			o.styleSize(), o.styleScale()
		}, this.styleSize = function() {
			if ((m = i.maxSourceWidth / h) < i.maxSourceHeight) return n < i.maxSourceWidth && (m = r), p();
			m = r > i.maxSourceHeight ? i.maxSourceHeight : r, p()
		}, this.styleScale = function() {
			i.isThumbing ? d.styleSourceUsingScaleAndHeight(i.thumbedSourcesOutersScale, u.current.offsetHeight) : 1 !== f[t] ? d.styleSourceUsingScaleAndHeight(f[t], c[t].current.offsetHeight) : d.ifSourcesScaledResetScale()
		};
		var p = function() {
			var e = a[t].current.style;
			e.width = m * h + "px", e.height = m + "px"
		}
	}

	function Q(e, t) {
		var n, r, o = e.elements.sources;
		this.translateY = function(e) {
			r = e, i()
		}, this.translateYAndScale = function(e, t) {
			r = e, n = t, i()
		};
		var i = function() {
			o[t].current.style.transform = "translateY(" + r + "px) scale(" + n + ")"
		}
	}

	function ee(e, t, n, r) {
		var o = this,
			i = e.componentsServices.isSourceLoadedCollection,
			s = e.collections,
			a = s.sourcesStylers,
			u = s.sourcesTransformers,
			c = e.data.initialAnimation,
			l = e.elements,
			f = l.sourcesInners,
			d = l.sources,
			h = e.resolve;
		this.runNormalLoadActions = function() {
			d[t].current.classList.add("fslightbox-opacity-1"), f[t].current.classList.add(c), i[t].set(!0), a[t].styleAll()
		}, this.runInitialLoadActions = function() {
			u[t] = h(Q, [t]), a[t] = h(K, [t, n, r]), o.runNormalLoadActions()
		}
	}

	function te(e, t) {
		var n, r = this,
			o = e.elements.sources,
			i = e.props.maxYoutubeVideoDimensions,
			s = e.resolve;
		this.handleImageLoad = function(e) {
			var t = e.target,
				n = t.width,
				o = t.height;
			r.handleImageLoad = a(n, o)
		}, this.handleVideoLoad = function(e) {
			var t = e.target,
				o = t.videoWidth,
				i = t.videoHeight;
			n = !0, r.handleVideoLoad = a(o, i)
		}, this.handleNotMetaDatedVideoLoad = function() {
			n || r.handleYoutubeLoad()
		}, this.handleYoutubeLoad = function() {
			var e = 1920,
				t = 1080;
			i && (e = i.width, t = i.height), r.handleYoutubeLoad = a(e, t)
		}, this.handleCustomLoad = function() {
			var e = o[t].current;
			r.handleCustomLoad = a(e.offsetWidth, e.offsetHeight)
		};
		var a = function(e, n) {
			var r = s(ee, [t, e, n]);
			return r.runInitialLoadActions(), r.runNormalLoadActions
		}
	}
	var ne = function(e) {
			var t = e.fsLightbox,
				n = t.core.thumbLoadHandler,
				r = t.elements,
				o = r.thumbs,
				i = r.thumbsWrappers,
				s = t.props.thumbsIcons,
				u = t.stageIndexes.current,
				c = e.src,
				l = e.i,
				f = "fslightbox-thumb";
			u === l && (f += " fslightbox-thumb-active");
			var d = s && s[l];
			return a.a.createElement("div", {
				ref: i[l],
				className: "fslightbox-thumb-wrapper"
			}, d, d && a.a.createElement("div", {
				className: "fslightbox-thumb-wrapper-darkener"
			}), a.a.createElement("img", {
				ref: o[l],
				className: f,
				src: c,
				alt: c,
				onLoad: n.handleLoad
			}))
		},
		re = function(e) {
			var t = e.fsLightbox,
				n = t.core.thumbLoadHandler,
				r = t.elements,
				o = r.thumbs,
				i = r.thumbsWrappers,
				u = t.stageIndexes.current,
				c = e.i;
			Object(s.useEffect)(n.handleLoad);
			var l = "fslightbox-thumb fslightbox-flex-centered";
			return c === u && (l += " fslightbox-thumb-active"), a.a.createElement("div", {
				ref: i[c],
				className: "fslightbox-thumb-invalid fslightbox-thumb-wrapper"
			}, a.a.createElement("div", {
				ref: o[c],
				className: l
			}, a.a.createElement(w, {
				viewBox: "0 0 30 30",
				height: "22px",
				width: "22px",
				d: "M15,3C8.373,3,3,8.373,3,15c0,6.627,5.373,12,12,12s12-5.373,12-12C27,8.373,21.627,3,15,3z M16.212,8l-0.2,9h-2.024l-0.2-9 H16.212z M15.003,22.189c-0.828,0-1.323-0.441-1.323-1.182c0-0.755,0.494-1.196,1.323-1.196c0.822,0,1.316,0.441,1.316,1.196 C16.319,21.748,15.825,22.189,15.003,22.189z"
			})))
		};

	function oe(e) {
		var t = e.componentsServices,
			n = e.data,
			r = e.elements.thumbsComponents,
			o = e.getState,
			i = e.props,
			s = i.showThumbsOnMount,
			u = i.sources,
			c = i.thumbs;
		this.buildComponentForTypeAndIndex = function(i, f) {
			c && c[f] ? r[f] = l(c, f) : r[f] = "image" === i ? l(u, f) : a.a.createElement(re, {
				fsLightbox: e,
				i: f,
				key: f
			}), o().isOpen && (s || n.isThumbing) && t.updateThumbsInner()
		};
		var l = function(t, n) {
			return a.a.createElement(ne, {
				fsLightbox: e,
				src: t[n],
				i: n,
				key: n
			})
		}
	}

	function ie(e) {
		var t, n = e.collections.sourcesLoadsHandlers,
			r = e.componentsServices.updateSourceInnerCollection,
			o = e.elements.sourcesComponents,
			i = e.getState,
			s = e.resolve,
			u = e.props.disableThumbs;
		u || (t = s(oe)), this.runActionsForSourceTypeAndIndex = function(c, l) {
			var f;
			switch ("invalid" !== c && (n[l] = s(te, [l])), c) {
				case "image":
					f = V;
					break;
				case "video":
					f = q;
					break;
				case "youtube":
					f = _;
					break;
				case "custom":
					f = $;
					break;
				default:
					f = G
			}
			o[l] = a.a.createElement(f, {
				fsLightbox: e,
				i: l
			}), u || t.buildComponentForTypeAndIndex(c, l), i().isOpen && r[l]()
		}
	}

	function se(e) {
		var t, n, r, o, i, s = e.collections.xhrs,
			a = {
				isUrlYoutubeOne: function(e) {
					var t = document.createElement("a");
					return t.href = e, "www.youtube.com" === t.hostname
				},
				getTypeFromResponseContentType: function(e) {
					return e.slice(0, e.indexOf("/"))
				}
			};
		this.setUrlToCheck = function(e) {
			t = e
		}, this.getSourceType = function(e) {
			if (a.isUrlYoutubeOne(t)) return e("youtube");
			r = e, o = new XMLHttpRequest, s.push(o), o.open("GET", t, !0), o.onreadystatechange = u, o.send()
		};
		var u = function() {
				if (4 === o.readyState && 0 === o.status && !i) return c();
				if (2 === o.readyState) {
					if (200 !== o.status && 206 !== o.status) return i = !0, c();
					i = !0, f(a.getTypeFromResponseContentType(o.getResponseHeader("content-type"))), l()
				}
			},
			c = function() {
				n = "invalid", l()
			},
			l = function() {
				o.abort(), r(n)
			},
			f = function(e) {
				switch (e) {
					case "image":
						n = "image";
						break;
					case "video":
						n = "video";
						break;
					default:
						n = "invalid"
				}
			}
	}

	function ae(e, t, n) {
		var r = e.props,
			o = r.types,
			i = r.type,
			s = r.sources,
			a = e.resolve;
		this.getTypeSetByClientForIndex = function(e) {
			var t;
			return o && o[e] ? t = o[e] : i && (t = i), t
		}, this.retrieveTypeWithXhrForIndex = function(e) {
			var r = a(se);
			r.setUrlToCheck(s[e]), r.getSourceType((function(r) {
				t.handleReceivedSourceTypeForUrl(r, s[e]), n.runActionsForSourceTypeAndIndex(r, e)
			}))
		}
	}

	function ue(e) {
		var t = e.data,
			n = e.core.eventsDispatcher;
		t.isInitialized = !0,
			function(e) {
				for (var t = e.data.sources, n = e.resolve, r = n(Z), o = n(ie), i = n(ae, [r, o]), s = 0; s < t.length; s++)
					if ("string" == typeof t[s]) {
						var a = i.getTypeSetByClientForIndex(s);
						if (a) o.runActionsForSourceTypeAndIndex(a, s);
						else {
							var u = r.getSourceTypeFromLocalStorageByUrl(t[s]);
							u ? o.runActionsForSourceTypeAndIndex(u, s) : i.retrieveTypeWithXhrForIndex(s)
						}
					} else o.runActionsForSourceTypeAndIndex("custom", s)
			}(e), n.dispatch("onInit")
	}

	function ce(e) {
		return e.touches ? e.touches[0].clientY : e.clientY
	}

	function le(e) {
		return e.touches ? e.touches.length : 0
	}

	function fe(e) {
		return Math.hypot(e.touches[0].pageX - e.touches[1].pageX, e.touches[0].pageY - e.touches[1].pageY)
	}

	function de(e, t) {
		var n = e.current.classList;
		n.contains(t) || n.add(t)
	}

	function he(e) {
		var t = e.core.zoomer,
			n = e.data,
			r = e.elements.sourcesInners,
			o = e.sourcesPointerProps,
			i = e.stageIndexes;
		this.runPinchActions = function(e) {
			o.isPinching = !0, o.pinchedHypot = fe(e), de(r[i.current], "fslightbox-source-inner-pinching"), 1 === n.zoom && t.startZooming()
		}
	}

	function me(e) {
		var t = e.core,
			n = t.lightboxCloser,
			r = t.lightboxOpener,
			o = t.slideIndexChanger,
			i = e.getState,
			s = e.stageIndexes;
		this.runTogglerUpdateActions = function() {
			i().isOpen ? n.close() : r.openLightbox()
		}, this.runCurrentStageIndexUpdateActionsFor = function(e) {
			e !== s.current && (i().isOpen ? o.jumpTo(e) : s.current = e)
		}
	}

	function pe(e) {
		return (pe = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
			return typeof e
		} : function(e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
		})(e)
	}

	function ge(e) {
		var t, n, r, o, i, s = e.props;
		this.setButtonName = function(e) {
			t = e
		}, this.setButtonComponent = function(e) {
			n = e
		}, this.setButtonData = function(e) {
			r = e
		}, this.getBuildedButton = function() {
			return u(), c(), a.a.createElement(n, {
				key: t,
				reference: r.reference,
				additionalClassName: i.additionalClassName,
				onClick: i.onClick,
				viewBox: i.viewBox,
				width: i.width,
				height: i.height,
				d: i.d,
				title: i.title
			})
		};
		var u = function() {
				o = n === T ? "toolbarButtons" : "slideButtons"
			},
			c = function() {
				if (i = r, "object" === pe(s.svg)) {
					var e = s.svg[o];
					if ("object" === pe(e)) {
						var n = e[t];
						"object" === pe(n) && X(i, n)
					}
				}
			}
	}

	function be(e) {
		return (be = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
			return typeof e
		} : function(e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
		})(e)
	}

	function ve(e) {
		var t, n, r, o, i, s, u, c, l, f = e.props,
			d = e.componentsServices.toolbarButtons;
		this.setButtonName = function(e) {
			n = e
		}, this.setButtonData = function(e) {
			t = e
		}, this.getBuildedButton = function() {
			return h(), m(), p(), b(), a.a.createElement(T, {
				onClick: v("onClick"),
				viewBox: v("viewBox"),
				width: v("width"),
				height: v("height"),
				d: v("d"),
				title: v("title")
			})
		};
		var h = function() {
				return "object" !== be(f.svg) || "object" !== be(f.svg.toolbarButtons) ? r = void 0 : void(r = f.svg.toolbarButtons[n])
			},
			m = function() {
				var e = Object.keys(t);
				o = e[0], i = e[1]
			},
			p = function() {
				if (s = t, !r) return g();
				"object" === be(r[o]) && X(s[o], r[o]), "object" === be(r[i]) && X(s[i], r[i]), g()
			},
			g = function() {
				u = s[o], c = s[i]
			},
			b = function() {
				l = d[n]
			},
			v = function(e) {
				return l.get() ? c[e] : u[e]
			}
	}
	var xe = function(e) {
		var t = e.additionalClassName,
			n = e.reference,
			o = e.onClick,
			i = e.viewBox,
			s = e.width,
			u = e.height,
			c = e.d,
			l = e.title;
		return a.a.createElement("div", {
			ref: n,
			onClick: o,
			title: l,
			className: "fslightbox-slide-btn-container " + t
		}, a.a.createElement("div", {
			className: r + "slide-btn fslightbox-flex-centered"
		}, a.a.createElement(w, {
			viewBox: i,
			width: s,
			height: u,
			d: c
		})))
	};

	function ye(e) {
		var t, n = e.collections.sourcesOutersTransformers,
			r = e.componentsServices.toolbarButtons.fullscreen,
			o = e.data,
			i = o.captionedSourcesOutersScales,
			s = o.sourcesOutersNoThumbsTranslatesY;
		this.handleFullscreenChange = function() {
			screen.height !== innerHeight && r.set(!1)
		}, this.scaleSourceOuterAtIndex = function(e) {
			t = e, a("scale")
		}, this.negativeAndScaleSourceOuterAtIndex = function(e) {
			t = e, a("negativeAndScale", "negative")
		};
		var a = function(e, r) {
			o.isThumbing ? n[t].translate(0, o.sourcesOutersThumbsTranslateY)[e](o.thumbedSourcesOutersScale) : i[t] ? n[t].translate(0, s[t])[e](i[t]) : r && n[t].negative()
		}
	}

	function Se(e, t) {
		var n = e.current.classList;
		n.contains(t) && n.remove(t)
	}

	function we(e) {
		var t = e.elements.captions;
		this.runSlideChangeActions = function(e, t) {
			n(e, "remove"), n(t, "add")
		};
		var n = function(e, n) {
			t[e].current && t[e].current.classList[n]("fslightbox-caption-active")
		}
	}

	function Te(e, t) {
		var n = [];
		return function() {
			n.push(!0), setTimeout((function() {
				n.shift(), n.length || e()
			}), t)
		}
	}

	function Ce(e) {
		return e.touches ? e.touches[0].clientX : e.clientX
	}

	function Le(e) {
		var t = e.componentsServices.toolbarButtons.slideshow,
			n = e.core,
			r = n.clickZoomer,
			o = n.lightboxCloser,
			i = n.fullscreenToggler,
			s = n.slideChangeFacade,
			a = n.slideshowManager,
			u = n.thumbsToggler,
			c = e.props.disableThumbs;
		this.listener = function(e) {
			if ("Space" !== e.code) switch (e.key) {
				case "Escape":
					o.close();
					break;
				case "ArrowLeft":
					s.changeToPrevious();
					break;
				case "ArrowRight":
					s.changeToNext();
					break;
				case "t":
					c || u.toggleThumbs();
					break;
				case "+":
					r.zoomIn();
					break;
				case "-":
					r.zoomOut();
					break;
				case "F11":
					e.preventDefault(), i.enterFullscreen()
			} else t.get() ? a.turnOffSlideshow() : a.turnOnSlideshow()
		}
	}

	function Ae(e) {
		var t = e.collections.sourcesOutersTransformers,
			n = e.core.zoomer,
			r = e.data,
			o = e.sourcesPointerProps;
		this.runZoomingPinchActionsForHypot = function(e) {
			var t = e - o.pinchedHypot,
				i = r.zoom + t / Math.hypot(innerWidth, innerHeight) * 10;
			i < .25 && (i = .25), n.zoomTo(i), o.pinchedHypot = e
		}, this.translateSourceOuterAtIndexUsingMethod = function(e, n) {
			t[e].translate(o.swipedX)[n]()
		}
	}

	function Ie(e) {
		var t = e.collections.sourcesOutersTransformers,
			n = e.componentsServices,
			r = e.core.pointeringBucket,
			o = e.resolve,
			i = e.sourcesPointerProps,
			s = e.stageIndexes,
			a = o(Ae);
		this.runActions = function(e) {
			r.runSwipingMoveActionsForPropsAndEvent(i, e), n.showSlideSwipingHovererIfNotYet()
		}, this.runPinchActions = function(e) {
			var t = fe(e);
			i.pinchedHypot ? a.runZoomingPinchActionsForHypot(t) : i.pinchedHypot = t
		}, this.runNormalSwipeActions = function() {
			a.translateSourceOuterAtIndexUsingMethod(s.current, "zero"), void 0 !== s.previous && i.swipedX > 0 ? a.translateSourceOuterAtIndexUsingMethod(s.previous, "negative") : void 0 !== s.next && i.swipedX < 0 && a.translateSourceOuterAtIndexUsingMethod(s.next, "positive")
		}, this.runZoomSwipeActions = function(e) {
			i.swipedX = Ce(e) - i.downClientX, i.swipedY = ce(e) - i.downClientY, t[s.current].translate(i.upSwipedX + i.swipedX, i.upSwipedY + i.swipedY).zero()
		}
	}

	function Oe(e) {
		var t, n = (t = !1, function() {
			return !t && (t = !0, requestAnimationFrame((function() {
				t = !1
			})), !0)
		});
		return function() {
			return e.isPointering && n()
		}
	}

	function Ee(e) {
		var t = e.data,
			n = e.resolve,
			r = e.sourcesPointerProps,
			o = Oe(r),
			i = n(Ie);
		this.listener = function(e) {
			o() && (i.runActions(e), le(e) && r.isPinching ? i.runPinchActions(e) : 1 === t.zoom ? 1 === t.sources.length ? r.swipedX = 1 : i.runNormalSwipeActions(e) : i.runZoomSwipeActions(e))
		}
	}

	function ze(e) {
		var t = e.collections.sourcesOutersTransformers,
			n = e.core,
			r = n.slideIndexChanger,
			o = n.clickZoomer,
			i = e.data,
			s = e.elements.sourcesOuters,
			a = e.sourcesPointerProps,
			u = e.stageIndexes;
		this.runPositiveSwipedXActions = function() {
			void 0 === u.previous || (c("positive"), r.changeTo(u.previous)), c("zero")
		}, this.runNegativeSwipedXActions = function() {
			void 0 === u.next || (c("negative"), r.changeTo(u.next)), c("zero")
		}, this.saveCurrentSourceOuterPosition = function() {
			a.upSwipedX = t[u.current].getTranslateX(), a.upSwipedY = t[u.current].getTranslateY()
		}, this.runSourceDownEventTargetActions = function() {
			i.zoom <= 1 ? o.zoomIn() : o.zoomOut()
		};
		var c = function(e) {
			s[u.current].current.classList.add("fslightbox-transform-transition"), t[u.current][e]()
		}
	}

	function Fe(e) {
		var t = e.componentsServices,
			n = e.core,
			r = n.lightboxCloser,
			o = n.pointeringBucket,
			i = e.data,
			s = e.elements.sourcesInners,
			a = e.resolve,
			u = e.sourcesPointerProps,
			c = e.stageIndexes,
			l = a(ze);
		this.runActions = function() {
			t.hideSlideSwipingHoverer(), u.isPinching = !1, u.pinchedHypot = 0, o.runSwipingTopActionsForPropsAndEvent(u), Se(s[c.current], "fslightbox-source-inner-pinching")
		}, this.runSwipeActions = function() {
			1 === i.zoom ? u.swipedX > 0 ? l.runPositiveSwipedXActions() : l.runNegativeSwipedXActions() : l.saveCurrentSourceOuterPosition()
		}, this.runNoSwipeActions = function() {
			u.isSourceDownEventTarget ? l.runSourceDownEventTargetActions() : r.close()
		}
	}

	function Pe(e) {
		var t = e.data,
			n = e.resolve,
			r = e.sourcesPointerProps,
			o = e.core.zoomer,
			i = n(Fe);
		this.listener = function(e) {
			r.isPointering && (r.isPinching || (r.swipedX ? i.runSwipeActions() : i.runNoSwipeActions()), i.runActions(e), t.zoom < 1 && (o.zoomTo(1), o.stopZooming()))
		}
	}

	function ke(e) {
		var t = e.componentsServices,
			n = e.core.pointeringBucket,
			r = e.data,
			o = e.elements.thumbsInner,
			i = e.thumbsSwipingProps;
		this.runActions = function(e) {
			n.runSwipingMoveActionsForPropsAndEvent(i, e), o.current.style.transform = "translateX(" + (r.thumbsTransform + i.swipedX) + "px)", t.showThumbsCursorerIfNotYet()
		}
	}

	function Be(e) {
		var t = e.data,
			n = e.resolve,
			r = Oe(e.thumbsSwipingProps),
			o = n(ke);
		this.listener = function(e) {
			t.thumbsInnerWidth > innerWidth && r() && o.runActions(e)
		}
	}

	function Me(e) {
		var t = e.componentsServices,
			n = e.data,
			r = e.core,
			o = r.slideIndexChanger,
			i = r.thumbsTransformTransitioner,
			s = r.pointeringBucket,
			a = e.elements,
			u = a.thumbsWrappers,
			c = a.thumbsInner,
			l = e.thumbsSwipingProps;
		this.runNoSwipeActionsForEvent = function(e) {
			t.hideThumbsCursorerIfShown(), l.isPointering = !1;
			for (var n = 0; n < u.length; n++)
				if (u[n].current && u[n].current.contains(e.target)) return void o.jumpTo(n)
		}, this.runActions = function() {
			if (t.hideThumbsCursorerIfShown(), n.thumbsTransform += l.swipedX, s.runSwipingTopActionsForPropsAndEvent(l), n.thumbsTransform > 0) return f(0);
			n.thumbsTransform < innerWidth - n.thumbsInnerWidth - 9 && f(innerWidth - n.thumbsInnerWidth - 9)
		};
		var f = function(e) {
			n.thumbsTransform = e, i.callActionWithTransition((function() {
				c.current.style.transform = "translateX(" + e + "px)"
			}))
		}
	}

	function Ne(e) {
		var t = e.resolve,
			n = e.thumbsSwipingProps,
			r = t(Me);
		this.listener = function(e) {
			n.isPointering && (n.swipedX ? r.runActions() : r.runNoSwipeActionsForEvent(e))
		}
	}

	function je(e) {
		var t = e.core.inactiver,
			n = e.props,
			r = e.resolve,
			o = r(Ee),
			i = r(Pe),
			s = r(Be),
			a = r(Ne);
		this.moveListener = function(e) {
			t.listener(e), o.listener(e), n.disableThumbs || s.listener(e)
		}, this.upListener = function(e) {
			i.listener(e), n.disableThumbs || a.listener(e)
		}
	}

	function Ue(e) {
		var t = e.collections,
			n = t.sourcesTransformers,
			r = t.sourcesOutersTransformers,
			o = e.data,
			i = e.elements,
			s = i.captions,
			a = i.slideButtonPrevious,
			u = i.slideButtonNext,
			c = i.sourcesOuters,
			l = i.thumbsContainer,
			f = e.stageIndexes;
		this.translateYSourceIfSupposed = function(e) {
			n[f.current] && o.sourcesTranslatesY[f.current] && n[f.current].translateY(e)
		}, this.runOpacity0ActionUsingMethod = function(e) {
			a.current && (a.current.classList[e]("fslightbox-opacity-0"), u.current.classList[e]("fslightbox-opacity-0"))
		}, this.translateYSourceOuterTo = function(e) {
			c[f.current].current.classList.add("fslightbox-transform-transition"), r[f.current].translate(0, e).zero()
		}, this.runActiveEnhancementActionUsingMethod = function(e) {
			o.isThumbing ? l.current.classList[e]("fslightbox-thumbs-active") : s[f.current].current && s[f.current].current.classList[e]("fslightbox-caption-active")
		}
	}

	function He(e) {
		var t, n, o, i, s, a;
		! function(e) {
			var t = e.core,
				n = t.clickZoomer,
				r = t.zoomer,
				o = e.data;
			n.zoomIn = function() {
				i(), a(o.zoom + .25), s()
			}, n.zoomOut = function() {
				.25 !== o.zoom && (i(), a(o.zoom - .25), s())
			};
			var i = function() {
					1 === o.zoom && r.startZooming()
				},
				s = function() {
					1 === o.zoom && r.stopZooming()
				},
				a = function(e) {
					o.zoom = e, r.zoomTo(o.zoom)
				}
		}(e), n = (t = e).core.buttonsBuildersFacade, o = t.elements, i = t.resolve, s = i(ge), a = i(ve), n.getOneStateToolbarButton = function(e, t) {
			return s.setButtonName(e), s.setButtonComponent(T), s.setButtonData(t), s.getBuildedButton()
		}, n.getOneStateSlideButton = function(e, t) {
			return s.setButtonName(e), s.setButtonComponent(xe), X(t, {
				additionalClassName: r + "slide-btn-container-" + e,
				reference: o["slideButton" + (e.charAt(0).toUpperCase() + e.slice(1))],
				viewBox: "0 0 20 20",
				width: "20px",
				height: "20px"
			}), s.setButtonData(t), s.getBuildedButton()
		}, n.getTwoStatesToolbarButton = function(e, t) {
			return a.setButtonName(e), a.setButtonData(t), a.getBuildedButton()
		},
			function(e) {
				var t = e.core.classFacade,
					n = e.elements;
				t.removeFromEachElementClassIfContains = function(e, t) {
					for (var r = 0; r < 100; r++)
						for (var o = 0; o < n[e].length; o++) Se(n[e][o], t)
				}
			}(e),
			function(e) {
				var t = e.core.eventsDispatcher,
					n = e.props;
				t.dispatch = function(t) {
					n[t] && n[t](e)
				}
			}(e),
			function(e) {
				var t = e.componentsServices.toolbarButtons.fullscreen,
					n = e.core.fullscreenToggler;
				n.enterFullscreen = function() {
					t.set(!0);
					var e = document.documentElement;
					e.requestFullscreen ? e.requestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.webkitRequestFullscreen ? e.webkitRequestFullscreen() : e.msRequestFullscreen && e.msRequestFullscreen()
				}, n.exitFullscreen = function() {
					t.set(!1), document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen()
				}
			}(e),
			function(e) {
				var t = e.core,
					n = t.inactiver,
					r = t.globalEventsController,
					o = t.windowResizeActioner,
					i = e.resolve,
					s = i(je),
					a = i(Le);
				r.addListeners = function() {
					U() ? (document.addEventListener("touchstart", n.listener, {
						passive: !0
					}), document.addEventListener("touchmove", H(s.moveListener, R), {
						passive: !0
					}), document.addEventListener("touchend", H(s.upListener, R))) : (document.addEventListener("mousedown", n.listener), document.addEventListener("mousemove", s.moveListener), document.addEventListener("mouseup", s.upListener)), addEventListener("resize", o.runActions), document.addEventListener("keydown", a.listener)
				}, r.removeListeners = function() {
					U() ? (document.removeEventListener("touchstart", n.listener), document.removeEventListener("touchmove", s.moveListener), document.removeEventListener("touchend", s.upListener)) : (document.removeEventListener("mousedown", n.listener), document.removeEventListener("mousemove", s.moveListener), document.removeEventListener("mouseup", s.upListener)), removeEventListener("resize", o.runActions), document.removeEventListener("keydown", a.listener)
				}
			}(e),
			function(e) {
				var t = e.core.inactiver,
					n = e.data,
					r = e.elements,
					o = r.nav,
					i = r.slideButtonPrevious,
					s = r.slideButtonNext,
					a = r.thumbsContainer,
					u = !1,
					c = Te((function() {
						u = !0, l(f)
					}), n.UIFadeOutTime);
				c(), t.listener = function() {
					c(), u && (l(d), u = !1)
				};
				var l = function(e) {
						try {
							e(o), 1 === n.zoom && i.current && (e(i), e(s)), n.isThumbing && e(a)
						} catch (e) {}
					},
					f = function(e) {
						e.current.classList.add("fslightbox-opacity-0")
					},
					d = function(e) {
						e.current.classList.remove("fslightbox-opacity-0")
					}
			}(e),
			function(e) {
				var t = e.core,
					n = t.lightboxCloser,
					r = t.lightboxCloseActioner;
				n.close = function() {
					r.isLightboxFadingOut || r.runActions()
				}
			}(e),
			function(e) {
				var t = e.componentsServices.toolbarButtons.fullscreen,
					n = e.core,
					r = n.eventsDispatcher,
					o = n.globalEventsController,
					i = n.fullscreenToggler,
					s = n.lightboxCloseActioner,
					a = n.scrollbarRecompensor,
					u = n.slideshowManager,
					c = n.zoomer,
					l = e.elements.container,
					f = e.props,
					d = e.setMainComponentState,
					h = e.sourcesPointerProps,
					m = e.thumbsSwipingProps;
				s.isLightboxFadingOut = !1, s.runActions = function() {
					s.isLightboxFadingOut = !0, l.current && l.current.classList.add("fslightbox-fade-out-strong"), o.removeListeners(), l.current && u.resetSlideshow(), l.current && t.get() && f.exitFullscreenOnClose && i.exitFullscreen(), c.ifZoomingResetZoom(), setTimeout((function() {
						s.isLightboxFadingOut = !1, h.isPointering = !1, m && (m.isPointering = !1), l.current && l.current.classList.remove("fslightbox-fade-out-strong"), document.documentElement.classList.remove("fslightbox-open"), a.removeRecompense(), l.current && d({
							isOpen: !1
						}, (function() {
							r.dispatch("onClose")
						}))
					}), 270)
				}
			}(e),
			function(e) {
				var t = e.setMainComponentState,
					n = e.core,
					r = n.lightboxOpener,
					o = n.lightboxOpenActioner;
				r.openLightbox = function() {
					t({
						isOpen: !0
					}, o.runActions)
				}
			}(e),
			function(e) {
				var t = e.collections.sourcesOutersTransformers,
					n = e.core,
					r = n.eventsDispatcher,
					o = n.lightboxOpenActioner,
					i = n.globalEventsController,
					s = n.scrollbarRecompensor,
					a = n.sourceDisplayFacade,
					u = n.stageManager,
					c = n.windowResizeActioner,
					l = e.data,
					f = e.stageIndexes;
				o.runActions = function() {
					l.unloadedThumbsCount = l.sources.length, u.updateStageIndexes(), document.documentElement.classList.add("fslightbox-open"), c.runActions(), s.addRecompense(), i.addListeners(), t[f.current].zero(), r.dispatch("onOpen"), l.isInitialized ? (r.dispatch("onShow"), a.displayStageSourcesIfNotYet()) : ue(e)
				}
			}(e),
			function(e) {
				var t, n, r = e.core.lightboxUpdater,
					o = e.data.sources,
					i = e.getProps,
					s = (0, e.resolve)(me),
					a = {
						setPrevProps: function(e) {
							t = e
						},
						setCurrProps: function(e) {
							n = e
						},
						hasTogglerPropChanged: function() {
							return t.toggler !== n.toggler
						},
						hasSlidePropChanged: function() {
							return void 0 !== n.slide && n.slide !== t.slide
						},
						hasSourcePropChanged: function() {
							return void 0 !== n.source && n.source !== t.source
						},
						hasSourceIndexPropChanged: function() {
							return void 0 !== n.sourceIndex && n.sourceIndex !== t.sourceIndex
						}
					};
				r.handleUpdate = function(e) {
					var t = i();
					a.setPrevProps(e), a.setCurrProps(t), a.hasTogglerPropChanged() && s.runTogglerUpdateActions(), a.hasSourcePropChanged() ? s.runCurrentStageIndexUpdateActionsFor(o.indexOf(t.source)) : a.hasSourceIndexPropChanged() ? s.runCurrentStageIndexUpdateActionsFor(t.sourceIndex) : a.hasSlidePropChanged() && s.runCurrentStageIndexUpdateActionsFor(t.slide - 1)
				}
			}(e),
			function(e) {
				var t = e.data,
					n = e.core.scrollbarRecompensor;
				n.addRecompense = function() {
					"complete" === document.readyState ? r() : window.addEventListener("load", (function() {
						r(), n.addRecompense = r
					}))
				};
				var r = function() {
					document.body.offsetHeight > window.innerHeight && (document.body.style.marginRight = t.scrollbarWidth + "px")
				};
				n.removeRecompense = function() {
					document.body.style.removeProperty("margin-right")
				}
			}(e),
			function(e) {
				var t = e.componentsServices.toolbarButtons.slideshow,
					n = e.core,
					r = n.slideshowManager,
					o = n.slideChangeFacade,
					i = e.data.slideshowTime,
					s = e.elements.slideshowBar,
					a = 0;
				r.turnOnSlideshow = function() {
					s.current.classList.add("fslightbox-opacity-1"), t.onUpdate = u, t.set(!0)
				}, r.turnOffSlideshow = function() {
					s.current.classList.remove("fslightbox-opacity-1"), t.set(!1)
				}, r.resetSlideshow = function() {
					a = 0, t.get() && t.set(!1)
				};
				var u = function e() {
					var n = (a += 16.67) / i;
					s.current.style.width = n * innerWidth + "px", n >= 1 && (a = 0, o.changeToNext()), t.get() && requestAnimationFrame(e)
				}
			}(e),
			function(e) {
				var t = e.core,
					n = t.slideChangeFacade,
					r = t.slideIndexChanger,
					o = t.stageManager;
				e.data.sources.length > 1 ? (n.changeToPrevious = function() {
					r.jumpTo(o.getPreviousSlideIndex())
				}, n.changeToNext = function() {
					r.jumpTo(o.getNextSlideIndex())
				}) : (n.changeToPrevious = function() {}, n.changeToNext = function() {})
			}(e),
			function(e) {
				var t = e.collections.sourcesOutersTransformers,
					n = e.componentsServices,
					r = e.core,
					o = r.classFacade,
					i = r.eventsDispatcher,
					s = r.slideIndexChanger,
					a = r.sourceDisplayFacade,
					u = r.stageManager,
					c = r.thumbsTransformer,
					l = r.zoomer,
					f = e.data,
					d = e.elements,
					h = d.sourcesInners,
					m = d.thumbs,
					p = e.props.disableThumbs,
					g = e.resolve,
					b = e.stageIndexes,
					v = f.initialAnimation,
					x = f.slideChangeAnimation,
					y = g(we),
					S = Te((function() {
						o.removeFromEachElementClassIfContains("sourcesInners", "fslightbox-fade-out")
					}), 300);
				s.changeTo = function(e) {
					p || (m[b.current].current && m[b.current].current.classList.remove("fslightbox-thumb-active"), m[e].current && m[e].current.classList.add("fslightbox-thumb-active")), f.isThumbing || y.runSlideChangeActions(b.current, e), l.ifZoomingResetZoom(), b.current = e, u.updateStageIndexes(), !p && m[b.current].current && c.transformToCurrentWithTransition(), n.setSlideNumber(e + 1), a.displayStageSourcesIfNotYet(), i.dispatch("onSlideChange")
				}, s.jumpTo = function(e) {
					var n = b.current;
					s.changeTo(e), o.removeFromEachElementClassIfContains("sourcesOuters", "fslightbox-transform-transition"), Se(h[n], v), Se(h[n], x), h[n].current.classList.add("fslightbox-fade-out"), Se(h[e], v), Se(h[e], "fslightbox-fade-out"), h[e].current.classList.add(x), S(), t[e].zero(), setTimeout((function() {
						n !== b.current && t[n].negative()
					}), 270)
				}
			}(e),
			function(e) {
				var t = e.core.sourceDisplayFacade,
					n = e.componentsServices.updateSourceInnerCollection,
					r = e.stageIndexes,
					o = e.props.loadOnlyCurrentSource;
				t.displayStageSourcesIfNotYet = function() {
					if (o) n[r.current]();
					else
						for (var e in r) void 0 !== r[e] && n[r[e]]()
				}
			}(e),
			function(e) {
				var t = e.core.sourceInnerTransitioner,
					n = e.elements.sourcesInners,
					r = e.stageIndexes;
				t.setUpPinchTransitionForCurrentSourceInner = function() {
					o("transform .1s linear")
				}, t.setUpZoomTransitionForCurrentSourceInner = function() {
					o("transform .3s")
				};
				var o = function(e) {
					n[r.current].style.transition = e
				}
			}(e),
			function(e) {
				var t = e.core,
					n = t.classFacade,
					r = t.sourcesPointerDown,
					o = t.pointeringBucket,
					i = e.elements.sources,
					s = e.resolve,
					a = e.sourcesPointerProps,
					u = e.stageIndexes,
					c = s(he);
				r.listener = function(e) {
					"VIDEO" === e.target.tagName || e.touches || e.preventDefault(), o.runSwipingDownActionsForPropsAndEvent(a, e), a.isMoveCallFirst = !0, a.downClientY = ce(e), 2 === le(e) ? c.runPinchActions(e) : n.removeFromEachElementClassIfContains("sourcesOuters", "fslightbox-transform-transition");
					var t = i[u.current].current;
					t && t.contains(e.target) ? a.isSourceDownEventTarget = !0 : a.isSourceDownEventTarget = !1
				}
			}(e),
			function(e) {
				var t = e.stageIndexes,
					n = e.core.stageManager,
					r = e.data.sources.length - 1;
				n.getPreviousSlideIndex = function() {
					return 0 === t.current ? r : t.current - 1
				}, n.getNextSlideIndex = function() {
					return t.current === r ? 0 : t.current + 1
				}, n.updateStageIndexes = 0 === r ? function() {} : 1 === r ? function() {
					0 === t.current ? (t.next = 1, delete t.previous) : (t.previous = 0, delete t.next)
				} : function() {
					t.previous = n.getPreviousSlideIndex(), t.next = n.getNextSlideIndex()
				}, n.isSourceInStage = r <= 2 ? function() {
					return !0
				} : function(e) {
					var n = t.current;
					if (0 === n && e === r || n === r && 0 === e) return !0;
					var o = n - e;
					return -1 === o || 0 === o || 1 === o
				}
			}(e),
			function(e) {
				var t = e.core.pointeringBucket,
					n = e.elements.container;
				t.runSwipingDownActionsForPropsAndEvent = function(e, t) {
					e.isPointering = !0, e.downClientX = Ce(t), e.swipedX = 0
				}, t.runSwipingMoveActionsForPropsAndEvent = function(e, t) {
					de(n, "fslightbox-cursor-grabbing"), e.swipedX = Ce(t) - e.downClientX
				}, t.runSwipingTopActionsForPropsAndEvent = function(e) {
					Se(n, "fslightbox-cursor-grabbing"), e.isPointering = !1
				}
			}(e),
			function(e) {
				var t = e.collections.sourcesStylers,
					n = e.core,
					r = n.windowResizeActioner,
					o = n.thumbsTransformer,
					i = e.componentsServices.toolbarButtons.fullscreen,
					s = e.data,
					a = e.elements,
					u = a.captions,
					c = a.sources,
					l = a.sourcesOuters,
					f = a.thumbs,
					d = a.thumbsContainer,
					h = e.props.disableThumbs,
					m = e.resolve,
					p = e.stageIndexes,
					g = m(ye),
					b = s.captionedSourcesOutersScales,
					v = s.sourcesOutersNoThumbsTranslatesY;
				r.runActions = function() {
					innerWidth < 992 ? s.maxSourceWidth = innerWidth : s.maxSourceWidth = .9 * innerWidth, s.maxSourceHeight = .9 * innerHeight, h || (s.thumbedSourcesOutersScale = 1 - d.current.offsetHeight / innerHeight, s.sourcesOutersThumbsTranslateY = -d.current.offsetHeight / 2), i.set && g.handleFullscreenChange(), 0 === s.unloadedThumbsCount && r.runThumbsActions();
					for (var e = 0; e < s.sources.length; e++) {
						if (u[e].current) {
							var n = u[e].current.offsetHeight - 25;
							b[e] = 1 - n / innerHeight, v[e] = -n / 2
						} else b[e] = 1, v[e] = 0;
						Se(l[e], "fslightbox-transform-transition"), e === p.current ? g.scaleSourceOuterAtIndex(e) : g.negativeAndScaleSourceOuterAtIndex(e), t[e] && c[e].current && t[e].styleAll()
					}
				}, r.runThumbsActions = function() {
					s.thumbsInnerWidth = 0;
					for (var e = 0; e < s.sources.length; e++) s.thumbsInnerWidth += f[e].current.offsetWidth + 8;
					o.transformToCurrent()
				}
			}(e),
			function(e) {
				var t = e.core.zoomer,
					n = e.data,
					r = e.elements,
					o = r.sources,
					i = r.sourcesInners,
					s = e.resolve,
					a = e.sourcesPointerProps,
					u = e.stageIndexes,
					c = s(Ue);
				t.zoomTo = function(e) {
					n.zoom = e, i[u.current].current.style.transform = "scale(" + e + ")"
				}, t.ifZoomingResetZoom = function() {
					1 !== n.zoom && (t.zoomTo(1), t.stopZooming())
				}, t.startZooming = function() {
					l("grab"), c.runOpacity0ActionUsingMethod("add"), c.runActiveEnhancementActionUsingMethod("remove"), c.translateYSourceOuterTo(0), c.translateYSourceIfSupposed(0)
				}, t.stopZooming = function() {
					l("zoom-in"), c.runOpacity0ActionUsingMethod("remove"), c.runActiveEnhancementActionUsingMethod("add");
					var e = n.sourcesOutersThumbsTranslateY;
					n.isThumbing || (e = n.sourcesOutersNoThumbsTranslatesY[u.current]), c.translateYSourceOuterTo(e), c.translateYSourceIfSupposed(n.sourcesTranslatesY[u.current]), a.upSwipedX = 0, a.upSwipedY = 0
				};
				var l = function(e) {
					o[u.current].current && (o[u.current].current.style.cursor = e)
				}
			}(e)
	}

	function Re(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return Ye(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return Ye(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function Ye(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var De = function(e) {
		var t = e.fsLightbox.componentsServices,
			n = Re(Object(s.useState)(!1), 2),
			o = n[0],
			i = n[1];
		return t.showSlideSwipingHovererIfNotYet = function() {
			o || i(!0)
		}, t.hideSlideSwipingHoverer = function() {
			i(!1)
		}, o && a.a.createElement("div", {
			className: r + "slide-swiping-hoverer fslightbox-full-dimension fslightbox-absoluted"
		})
	};

	function We() {
		var e = localStorage.getItem("fslightbox-scrollbar-width");
		if (e) return e;
		var t = function() {
				var e = document.createElement("div"),
					t = e.style;
				return t.visibility = "hidden", t.width = "100px", t.msOverflowStyle = "scrollbar", t.overflow = "scroll", e
			}(),
			n = function() {
				var e = document.createElement("div");
				return e.style.width = "100%", e
			}();
		document.body.appendChild(t);
		var r = t.offsetWidth;
		t.appendChild(n);
		var o = n.offsetWidth;
		document.body.removeChild(t);
		var i = r - o;
		return localStorage.setItem("fslightbox-scrollbar-width", i.toString()), i
	}

	function Xe(e) {
		var t = e.core.lightboxOpenActioner.runActions,
			n = e.data,
			r = e.props.openOnMount;
		document.getElementsByClassName("fslightbox-styles").length || o(), n.scrollbarWidth = We(), r && t()
	}
	var Ze = function(e) {
		var t = e.fsLightbox.elements.slideshowBar;
		return a.a.createElement("div", {
			ref: t,
			className: r + "slideshow-bar fslightbox-absoluted"
		})
	};

	function Ve(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return qe(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return qe(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function qe(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var _e = function(e) {
		var t = e.fsLightbox,
			n = t.componentsServices,
			r = t.core.thumbsSwipingDown.listener,
			o = t.elements,
			i = o.thumbsComponents,
			u = o.thumbsInner,
			c = Ve(Object(s.useState)(!1), 2),
			l = c[0],
			f = c[1],
			d = new Y;
		return n.updateThumbsInner = function() {
			f(!l)
		}, a.a.createElement("div", {
			className: "fslightbox-thumbs-inner",
			ref: u,
			onMouseDown: d.getMouseDownListenerFunc(r),
			onTouchStart: d.getTouchStartListenerForFunc(r)
		}, i)
	};

	function $e(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return Ge(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return Ge(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function Ge(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var Je = function(e) {
		var t = e.fsLightbox.componentsServices,
			n = $e(Object(s.useState)(!1), 2),
			r = n[0],
			o = n[1];
		return t.showThumbsCursorerIfNotYet = function() {
			r || o(!0)
		}, t.hideThumbsCursorerIfShown = function() {
			r && o(!1)
		}, r ? a.a.createElement("div", {
			className: "fslightbox-thumbs-cursorer fslightbox-full-dimension fslightbox-absoluted"
		}) : null
	};

	function Ke(e, t) {
		return function(e) {
			if (Array.isArray(e)) return e
		}(e) || function(e, t) {
			if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;
			var n = [],
				r = !0,
				o = !1,
				i = void 0;
			try {
				for (var s, a = e[Symbol.iterator](); !(r = (s = a.next()).done) && (n.push(s.value), !t || n.length !== t); r = !0);
			} catch (e) {
				o = !0, i = e
			} finally {
				try {
					r || null == a.return || a.return()
				} finally {
					if (o) throw i
				}
			}
			return n
		}(e, t) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return Qe(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return Qe(e, t)
		}(e, t) || function() {
			throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function Qe(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}
	var et = function(e) {
		var t = e.fsLightbox,
			n = t.componentsServices,
			r = t.data.isThumbing,
			o = t.elements.thumbsContainer,
			i = Ke(Object(s.useState)(!0), 2),
			u = i[0],
			c = i[1];
		n.hideThumbsLoader = function() {
			return c(!1)
		};
		var l = "fslightbox-thumbs";
		return r && (l += " fslightbox-thumbs-active"), a.a.createElement("div", {
			className: l,
			ref: o
		}, a.a.createElement(Je, {
			fsLightbox: t
		}), a.a.createElement(_e, {
			fsLightbox: t
		}), u && a.a.createElement(F, {
			testId: "thumbs-loader",
			additionalClassName: "fslightbox-thumbs-loader",
			key: "loader"
		}))
	};

	function tt(e) {
		var t = e.collections,
			n = t.sourcesOutersTransformers,
			r = t.sourcesStylers,
			o = e.componentsServices,
			i = e.core.zoomer,
			s = e.data,
			a = e.elements,
			u = a.captions,
			c = a.sources,
			l = a.sourcesOuters,
			f = a.thumbsContainer,
			d = e.stageIndexes,
			h = s.captionedSourcesOutersScales,
			m = s.sources.length;
		this.openThumbs = function() {
			i.ifZoomingResetZoom(), f.current.classList.add("fslightbox-thumbs-active"), p("remove"), s.isThumbing = !0;
			for (var e = 0; e < m; e++) g(e), n[e].translate(0, s.sourcesOutersThumbsTranslateY).scale(s.thumbedSourcesOutersScale);
			o.updateThumbsInner()
		}, this.closeThumbs = function() {
			i.ifZoomingResetZoom(), f.current.classList.remove("fslightbox-thumbs-active"), p("add"), s.isThumbing = !1;
			for (var e = 0; e < m; e++) g(e), h[e] && n[e].translate(0, s.sourcesOutersNoThumbsTranslatesY[e]).scale(h[e])
		};
		var p = function(e) {
				u[d.current].current && u[d.current].current.classList[e]("fslightbox-caption-active")
			},
			g = function(e) {
				r[e] && c[e].current && r[e].styleScale(), de(l[e], "fslightbox-transform-transition")
			}
	}

	function nt(e) {
		var t = e.data,
			n = e.elements,
			r = n.thumbsWrappers,
			o = n.thumbsContainer,
			i = n.thumbsInner,
			s = e.stageIndexes;
		this.runActions = function() {
			Se(o, "fslightbox-flex-centered");
			var e = innerWidth / 2,
				n = r[s.current].current,
				i = n.offsetLeft + n.offsetWidth / 2,
				u = t.thumbsInnerWidth - i;
			i > e && u > e ? a(e - i) : i > e ? a(innerWidth - t.thumbsInnerWidth - 9) : u > e && a(0)
		}, this.runToThinThumbsActions = function() {
			de(o, "fslightbox-flex-centered"), a(0)
		};
		var a = function(e) {
			t.thumbsTransform = e, i.current.style.transform = "translateX(" + e + "px)"
		}
	}

	function rt(e) {
		var t, n, r, o, i = e.componentsServices,
			s = e.core,
			u = e.data,
			c = e.elements,
			l = e.props;
		i.updateThumbsInner = null, i.showThumbsCursorerIfNotYet = null, i.hideThumbsCursorerIfShown = null, i.hideThumbsLoader = null, u.thumbsInnerWidth = null, u.unloadedThumbsCount = u.sources.length, u.thumbsTransform = 0, u.isThumbing = l.showThumbsOnMount, u.thumbedSourcesOutersScale = null, e.thumbsSwipingProps = {
			isPointering: !1,
			downClientX: null,
			swipedX: null
		}, s.thumbLoadHandler = {}, s.thumbsOpeningActions = {}, s.thumbsTransformer = {}, s.thumbsTransformTransitioner = {}, s.thumbsToggler = {}, s.thumbsSwipingDown = {}, c.thumbsContainer = a.a.createRef(), c.thumbs = W(e), c.thumbsWrappers = W(e), c.thumbsComponents = [], c.thumbsInner = a.a.createRef(),
			function(e) {
				var t = e.core,
					n = t.thumbLoadHandler,
					r = t.windowResizeActioner,
					o = e.componentsServices,
					i = e.data,
					s = e.elements.thumbsWrappers;
				n.handleLoad = function() {
					if (i.unloadedThumbsCount--, 0 === i.unloadedThumbsCount) {
						for (var e = 0; e < s.length; e++) s[e].current.classList.add("fslightbox-opacity-1");
						r.runThumbsActions(), o.hideThumbsLoader()
					}
				}
			}(e),
			function(e) {
				var t = e.core.thumbsToggler,
					n = e.data,
					r = (0, e.resolve)(tt);
				t.toggleThumbs = function() {
					n.isThumbing ? r.closeThumbs() : r.openThumbs()
				}
			}(e),
			function(e) {
				var t = e.core,
					n = t.thumbsTransformer,
					r = t.thumbsTransformTransitioner,
					o = e.data,
					i = (0, e.resolve)(nt);
				n.transformToCurrent = function() {
					o.thumbsInnerWidth > innerWidth ? i.runActions() : i.runToThinThumbsActions()
				}, n.transformToCurrentWithTransition = function() {
					o.thumbsInnerWidth > innerWidth && r.callActionWithTransition(i.runActions)
				}
			}(e), n = (t = e).core.thumbsTransformTransitioner, r = t.elements.thumbsInner, o = Te((function() {
			r.current.classList.remove("fslightbox-transform-transition")
		}), 300), n.callActionWithTransition = function(e) {
			r.current.classList.add("fslightbox-transform-transition"), e(), o()
		},
			function(e) {
				var t = e.core,
					n = t.thumbsSwipingDown,
					r = t.pointeringBucket,
					o = e.thumbsSwipingProps;
				n.listener = function(e) {
					r.runSwipingDownActionsForPropsAndEvent(o, e), e.touches || e.preventDefault()
				}
			}(e)
	}

	function ot(e, t) {
		var n = this,
			r = e.elements.sourcesOuters,
			o = e.data,
			i = 0,
			s = 0,
			a = 0,
			u = 0;
		this.translate = function(e, t) {
			return s = e, void 0 !== t && (a = t), n
		}, this.getTranslateX = function() {
			return i
		}, this.getTranslateY = function() {
			return a
		}, this.negative = function() {
			c(-(1 + o.slideDistance) * innerWidth)
		}, this.zero = function() {
			c(0)
		}, this.positive = function() {
			c((1 + o.slideDistance) * innerWidth)
		}, this.scale = function(e) {
			u = e, l()
		}, this.negativeAndScale = function(e) {
			u = e, n.negative()
		};
		var c = function(e) {
				i = e + s, l(), s = 0
			},
			l = function() {
				f(u ? "translate(" + i + "px, " + a + "px) scale(" + u + ")" : "translate(" + i + "px, " + a + "px)")
			},
			f = function(e) {
				r[t].current.style.transform = e
			}
	}

	function it(e) {
		for (var t = e.data.sources, n = e.resolve, r = [], o = 0; o < t.length; o++) r[o] = n(ot, [o]);
		return r
	}
	var st = function(e) {
			var t = e.fsLightbox,
				n = e.index,
				r = e.children,
				o = t.data.isThumbing,
				i = t.elements.captions,
				s = "fslightbox-caption";
			return n !== t.stageIndexes.current || o || (s += " fslightbox-caption-active"), a.a.createElement("div", {
				ref: i[n],
				className: s + " fslightbox-flex-centered"
			}, a.a.createElement("div", {
				className: "fslightbox-caption-inner"
			}, r))
		},
		at = function(e) {
			var t = e.fsLightbox,
				n = t.props.captions;
			return n ? n.map((function(e, n) {
				return e && a.a.createElement(st, {
					fsLightbox: t,
					index: n,
					key: n
				}, e)
			})) : null
		};

	function ut(e) {
		var t = e.props,
			n = t.sources,
			r = t.customSources,
			o = n ? n.slice() : r.slice();
		if (r && n !== r)
			for (var i = 0; i < r.length; i++) r[i] && (o[i] = r[i]);
		return o
	}

	function ct(e) {
		return (ct = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
			return typeof e
		} : function(e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
		})(e)
	}

	function lt(e, t, n) {
		return (lt = bt() ? Reflect.construct : function(e, t, n) {
			var r = [null];
			r.push.apply(r, t);
			var o = new(Function.bind.apply(e, r));
			return n && mt(o, n.prototype), o
		}).apply(null, arguments)
	}

	function ft(e) {
		return function(e) {
			if (Array.isArray(e)) return dt(e)
		}(e) || function(e) {
			if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) return Array.from(e)
		}(e) || function(e, t) {
			if (!e) return;
			if ("string" == typeof e) return dt(e, t);
			var n = Object.prototype.toString.call(e).slice(8, -1);
			"Object" === n && e.constructor && (n = e.constructor.name);
			if ("Map" === n || "Set" === n) return Array.from(e);
			if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return dt(e, t)
		}(e) || function() {
			throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
		}()
	}

	function dt(e, t) {
		(null == t || t > e.length) && (t = e.length);
		for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
		return r
	}

	function ht(e, t) {
		for (var n = 0; n < t.length; n++) {
			var r = t[n];
			r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
		}
	}

	function mt(e, t) {
		return (mt = Object.setPrototypeOf || function(e, t) {
			return e.__proto__ = t, e
		})(e, t)
	}

	function pt(e, t) {
		return !t || "object" !== ct(t) && "function" != typeof t ? gt(e) : t
	}

	function gt(e) {
		if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		return e
	}

	function bt() {
		if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
		if (Reflect.construct.sham) return !1;
		if ("function" == typeof Proxy) return !0;
		try {
			return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}))), !0
		} catch (e) {
			return !1
		}
	}

	function vt(e) {
		return (vt = Object.setPrototypeOf ? Object.getPrototypeOf : function(e) {
			return e.__proto__ || Object.getPrototypeOf(e)
		})(e)
	}
	var xt = function(e) {
		! function(e, t) {
			if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
			e.prototype = Object.create(t && t.prototype, {
				constructor: {
					value: e,
					writable: !0,
					configurable: !0
				}
			}), t && mt(e, t)
		}(c, e);
		var t, n, o, i, s, u = (t = c, n = bt(), function() {
			var e, r = vt(t);
			if (n) {
				var o = vt(this).constructor;
				e = Reflect.construct(r, arguments, o)
			} else e = r.apply(this, arguments);
			return pt(this, e)
		});

		function c(e) {
			var t;
			return function(e, t) {
				if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
			}(this, c), (t = u.call(this, e)).setUpData(), t.setUpStageIndexes(), t.setUpStates(), t.setUpGetters(), t.setUpSetters(), t.setUpElements(), t.setUpResolve(), t.setUpCoreSkeleton(), t.setUpCollections(), e.disableThumbs || rt(gt(t)), He(gt(t)), t
		}
		return o = c, (i = [{
			key: "setUpData",
			value: function() {
				this.data = {
					sources: ut(this),
					isInitialized: !1,
					maxSourceWidth: 0,
					maxSourceHeight: 0,
					scrollbarWidth: 0,
					slideDistance: this.props.slideDistance ? this.props.slideDistance : .3,
					slideshowTime: this.props.slideshowTime ? this.props.slideshowTime : 8e3,
					UIFadeOutTime: this.props.UIFadeOutTime ? this.props.UIFadeOutTime : 8e3,
					isSourceDisplayedArray: [],
					captionedSourcesOutersScales: [],
					sourcesTranslatesY: [],
					sourcesOutersThumbsTranslateY: null,
					sourcesOutersNoThumbsTranslatesY: [],
					zoom: 1,
					initialAnimation: this.props.initialAnimation ? this.props.initialAnimation : "fslightbox-fade-in-strong",
					slideChangeAnimation: this.props.slideChangeAnimation ? this.props.slideChangeAnimation : "fslightbox-fade-in"
				}, this.sourcesPointerProps = {
					isPointering: !1,
					downClientX: null,
					downClientY: null,
					isSourceDownEventTarget: !1,
					isMoveCallFirst: !1,
					swipedX: 0,
					swipedY: 0,
					upSwipedX: 0,
					upSwipedY: 0,
					pinchedHypot: 0
				}
			}
		}, {
			key: "setUpStageIndexes",
			value: function() {
				var e, t, n, r, o, i, s;
				this.stageIndexes = {
					previous: void 0,
					current: (e = this, t = e.data.sources, n = e.props, r = n.slide, o = n.sourceIndex, i = n.source, s = 0, i ? s = t.indexOf(i) : o ? s = o : r && (s = r - 1), s),
					next: void 0
				}
			}
		}, {
			key: "setUpStates",
			value: function() {
				this.state = {
					isOpen: this.props.openOnMount
				}, this.componentsServices = {
					showSlideSwipingHovererIfNotYet: null,
					hideSlideSwipingHoverer: null,
					isSourceLoadedCollection: [],
					updateSourceInnerCollection: [],
					isFullscreenOpen: {},
					isSlideShowOn: {},
					slideNumberUpdater: {},
					toolbarButtons: {
						fullscreen: {},
						slideshow: {}
					}
				}
			}
		}, {
			key: "setUpGetters",
			value: function() {
				var e = this;
				this.getProps = function() {
					return e.props
				}, this.getState = function() {
					return e.state
				}
			}
		}, {
			key: "setUpSetters",
			value: function() {
				var e = this;
				this.setMainComponentState = function(t, n) {
					return e.setState(t, n)
				}
			}
		}, {
			key: "setUpElements",
			value: function() {
				this.elements = {
					captions: W(this),
					container: a.a.createRef(),
					nav: a.a.createRef(),
					slideButtonPrevious: a.a.createRef(),
					slideButtonNext: a.a.createRef(),
					slideshowBar: a.a.createRef(),
					sourcesOutersWrapper: a.a.createRef(),
					sources: W(this),
					sourcesInners: W(this),
					sourcesOuters: W(this),
					sourcesComponents: []
				}
			}
		}, {
			key: "setUpResolve",
			value: function() {
				var e = this;
				this.resolve = function(t) {
					var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
					return n.unshift(e), lt(t, ft(n))
				}
			}
		}, {
			key: "setUpCoreSkeleton",
			value: function() {
				this.core = {
					buttonsBuildersFacade: {},
					classFacade: {},
					clickZoomer: {},
					eventsDispatcher: {},
					fullscreenToggler: {},
					globalEventsController: {},
					inactiver: {},
					lightboxCloser: {},
					lightboxCloseActioner: {},
					lightboxOpener: {},
					lightboxOpenActioner: {},
					lightboxUpdater: {},
					scrollbarRecompensor: {},
					slideshowManager: {},
					slideChangeFacade: {},
					slideIndexChanger: {},
					sourceDisplayFacade: {},
					sourceInnerTransitioner: {},
					sourcesPointerDown: {},
					stageManager: {},
					pointeringBucket: {},
					windowResizeActioner: {},
					zoomer: {}
				}
			}
		}, {
			key: "setUpCollections",
			value: function() {
				this.collections = {
					sourcesOutersTransformers: it(this),
					sourcesTransformers: [],
					sourcesLoadsHandlers: [],
					sourcesStylers: [],
					xhrs: []
				}
			}
		}, {
			key: "componentDidUpdate",
			value: function(e) {
				this.core.lightboxUpdater.handleUpdate(e)
			}
		}, {
			key: "componentDidMount",
			value: function() {
				Xe(this)
			}
		}, {
			key: "componentWillUnmount",
			value: function() {
				! function(e) {
					for (var t = e.collections.xhrs, n = e.core.lightboxCloseActioner, r = e.getState, o = 0; o < t.length; o++) t[o].abort();
					r().isOpen && n.runActions()
				}(this)
			}
		}, {
			key: "render",
			value: function() {
				return this.state.isOpen ? a.a.createElement("div", {
					ref: this.elements.container,
					className: r + "container fslightbox-full-dimension fslightbox-fade-in-strong"
				}, a.a.createElement(De, {
					fsLightbox: this
				}), a.a.createElement(O, {
					fsLightbox: this
				}), a.a.createElement(Ze, {
					fsLightbox: this
				}), this.data.sources.length > 1 ? a.a.createElement(a.a.Fragment, null, a.a.createElement(E, {
					fsLightbox: this
				}), a.a.createElement(z, {
					fsLightbox: this
				})) : null, a.a.createElement(D, {
					fsLightbox: this
				}), a.a.createElement(at, {
					fsLightbox: this
				}), this.props.disableThumbs ? null : a.a.createElement(et, {
					fsLightbox: this
				})) : null
			}
		}]) && ht(o.prototype, i), s && ht(o, s), c
	}(s.Component);
	xt.propTypes = {
		toggler: c.a.bool.isRequired,
		sources: c.a.array.isRequired,
		customSources: c.a.array,
		initialAnimation: c.a.string,
		slideChangeAnimation: c.a.string,
		captions: c.a.array,
		onOpen: c.a.func,
		onClose: c.a.func,
		onInit: c.a.func,
		onShow: c.a.func,
		onSlideChange: c.a.func,
		source: c.a.string,
		sourceIndex: c.a.number,
		slide: c.a.number,
		maxYoutubeVideoDimensions: c.a.array,
		videosPosters: c.a.array,
		svg: c.a.object,
		disableLocalStorage: c.a.bool,
		types: c.a.array,
		type: c.a.string,
		thumbs: c.a.array,
		disableThumbs: c.a.bool,
		showThumbsOnMount: c.a.bool,
		thumbsIcons: c.a.array,
		customToolbarButtons: c.a.array,
		loadOnlyCurrentSource: c.a.bool,
		slideDistance: c.a.number,
		slideshowTime: c.a.number,
		UIFadeOutTime: c.a.number,
		openOnMount: c.a.bool,
		exitFullscreenOnClose: c.a.bool
	};
	t.default = xt
}]);