// import IframeResizer from 'iframe-resizer-react'
import React, { useRef, useState } from 'react'
import loadable from '@loadable/component'

const IframeComponent = loadable(() => import('iframe-resizer-react'))


const Iframe = (props) => {
  const ref = useRef(null)
  const [messageData, setMessageData] = useState(undefined)

  const onResized = (data) => setMessageData(data)

  const onMessage = (data) => {
    setMessageData(data)
    ref.current.sendMessage('Hello back from parent page')
  }

  return (
    <>
      {typeof window !== 'undefined' ? (
        <IframeComponent
          log
          heightCalculationMethod={props.mode}
          inPageLinks
          // bodyMargin={'0 0 5rem 0'}
          forwardRef={ref}
          onMessage={onMessage}
          onResized={onResized}
          src={props.src}
          width="100%"
          sizeHeight
          scrolling={false}
          checkOrigin={false}
        />
      ) : (
        <p>IFRAME</p>
        )
      }
    </>
  )
}

export default Iframe