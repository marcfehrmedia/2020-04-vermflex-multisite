import React, { useState } from 'react'
import FsLightbox from 'fslightbox-react'
import styled from 'styled-components'
import 'semantic-ui-css/components/button.min.css'
import 'semantic-ui-css/components/icon.min.css'

const StyledVideo = styled.div`
	position: relative;
	width: 100%;
	margin-top: 1.5rem;

	video {
		width: 100%;
	}
	
	p {
		color: ${props => props.theme.colors.secondary};
		line-height: 1.15;
		-webkit-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    margin-top: 0.9rem;
	}
	
	&:hover { cursor: pointer; }
`

const PlayButton = styled.button`
	position: absolute;
	top: calc(50% - 23px);
	left: calc(50% - 23px);
	margin: 0 !important;
	background: ${props => props.theme.colors.highlight} !important;
	pointer-events: none;
	
	i { margin: 0 !important; }
	
	&:hover { cursor: pointer; }
`

export default function VideoPlayer(props) {
	const [lightboxController, setLightboxController] = useState({
		toggler: false,
		slide: 1
	});

	function openLightboxOnSlide(number) {
		setLightboxController({
			toggler: !lightboxController.toggler,
			slide: number
		});
	}


	return (
		<StyledVideo>
			<div
				style={{position: 'relative'}}
				onClick={() => openLightboxOnSlide(1)}
			>
				<FsLightbox
					toggler={lightboxController.toggler}
					sources={ [
						props.src
					] }
				/>
				<video>
					<source src={props.src} type={'video/mp4'} />
				</video>
				<PlayButton
					className={'ui circular big icon button'}
				>
					<i className={'icon play'} />
				</PlayButton>
			</div>
			<p>{ props.description }</p>
		</StyledVideo>
	)
}