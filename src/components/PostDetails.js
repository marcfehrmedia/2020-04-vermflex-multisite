import React from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/components/table.css'

const Wrapper = styled.div`
  margin: 0 auto 2em;
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  span {
    margin: 0 0.5rem;
  }
`

const Date = styled.p`
  display: inline-block;
`

const ReadingTime = styled.p`
  display: inline-block;
`

const PostDetails = props => {
  return (
    <Wrapper>
      <table className="ui very basic very small table">
        <tbody>
          {props.vehicle && (
            <tr>
              <td>Fahrzeug</td>
              <td>{props.vehicle}</td>
            </tr>
          )}
          {props.doneBy && (
            <tr>
              <td>Fertigstellung:</td>
              <td>{props.doneBy}</td>
            </tr>
          )}
          {props.dauer && (
            <tr>
              <td>Umsetzungsdauer:</td>
              <td>{props.dauer}</td>
            </tr>
          )}
          {props.customer && (
            <tr>
              <td>Kunde:</td>
              <td>
                <a
                  href={`https://www.instagram.com/${props.customer}`}
                  target="_blank"
                  rel="noopener"
                >
                  {props.customer !== ' ' && <i className="ui icon instagram" /> }
                  {props.customer}
                </a>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </Wrapper>
  )
}

export default PostDetails
