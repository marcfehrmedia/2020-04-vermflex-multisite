import React from "react"
import renderer from "react-test-renderer"

import Wrapper from "../container"

describe("Wrapper", () => {
	it("renders correctly", () => {
		const tree = renderer
			.create(<section fullHeight />)
			.toJSON()
		expect(tree).toMatchSnapshot()
	})
})
