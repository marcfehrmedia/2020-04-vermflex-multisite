import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import 'semantic-ui-css/components/segment.css'
import 'semantic-ui-css/components/label.css'

const List = styled.ul`
  width: 100%;
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  &:not(.inline) {
    margin: 0 auto 1em auto;
  }
`

const TagButton = styled.button `
  margin-bottom: 5px !important;
`

const Tag = styled.button`
  display: ${props => (props.inline ? 'inline' : 'inline-block')};
  margin: 0 0.25em 0.25em 0;
  a {
    float: left;
    transition: 0.2s;
    background: ${props => props.theme.colors.tertiary};
    padding: 0.5em;
    border-radius: 2px;
    text-transform: capitalize;
    text-decoration: none;
    color: ${props => props.theme.colors.base};
    border: 1px solid ${props => props.theme.colors.secondary};
    &:hover {
      background: ${props => props.theme.colors.secondary};
    }
  }
`

const LabelContainer = styled.div`
  padding: .5rem;
  margin-bottom: 0;
`

const Label = styled.button`
  margin-bottom: 4px !important;
  border-radius: 0 !important;
  color: rgba(0,0,0,0.8) !important;
  * {
    color: rgba(0,0,0,0.8) !important;
  }
`

const TagList = props => {
  return (
    <List className={props.inline ? 'inline' : ''}>
      {!props.inline && (
        <Link to="/blog">
          <Label className="ui tiny basic icon button">
            <i className="ui icon long arrow left" />
            Alle Beiträge
          </Label>
        </Link>
      )}
      {props.inline ? (
        <LabelContainer>
          {props.tags.map((tag, i) => (
            <Label className="ui tiny button" key={tag.id}>
              <Link key={`tag-${i}`} to={`/tag/${tag.slug}/`}>
                {tag.title}
              </Link>
            </Label>
          ))}
        </LabelContainer>
      ) : (
        props.tags.map((tag, i) => (
          <Link key={`tag-${i}`} to={`/tag/${tag.slug}/`}>
            <Label className="ui tiny button" key={tag.id}>
              {tag.title}
            </Label>
          </Link>
        ))
      )}
    </List>
  )
}

export default TagList
