import React from 'react'
import { Link } from 'gatsby'
import 'semantic-ui-css/components/button.css'

const SocialButton = props => {
  if (props.external) {
    return (
      <a
        rel="noopener"
        href={props.url}
        target={'_blank'}
        className={`ui social-button button ${props.additionalClasses} ${props.size} ${props.color} ${props.service} ${props.inverted ? 'inverted' : ''}`}
      >
        <i className={`ui icon ${props.service}`} />
        {props.title}
      </a>
    )
  } else {
    return (
      <Link
        to={props.url}
        className={`ui social-button button ${props.additionalClasses} ${props.size} ${props.color} ${props.service} ${props.inverted ? 'inverted' : ''}`}
      >
        <i className={`ui icon ${props.service}`} />
        {props.title}
      </Link>
    )
  }
}

export default SocialButton
