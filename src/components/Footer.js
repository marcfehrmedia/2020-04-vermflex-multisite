import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import Container from './Container'
import { particleConfig } from '../utils/particles'

import 'semantic-ui-css/components/icon.css'
import 'semantic-ui-css/components/button.css'

import loadable from '@loadable/component'
const ParticleComponent = loadable(() => import('react-tsparticles'))

const Wrapper = styled.footer`
  display: flex;
  position: relative;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  margin: 0 auto;
  width: 100%;
  overflow: hidden;
  background: ${props => props.theme.colors.secondary};
`

const List = styled.ul`
  max-width: ${props => props.theme.sizes.maxWidth};
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  // border-top: 1px solid ${props => props.theme.colors.secondary};
  padding: 0;
  margin: 0 auto;
`

const StyledButton = styled.button`
  &.circular.ui.button {
    background: ${props => props.theme.colors.highlight};
  }
`

const Item = styled.li`
  display: inline-block;
  z-index: 10;
  padding: 0.25em 0;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    width: auto;
  }
  h4 {
    font-size: 1.1em;
    font-family: ${props => props.theme.fonts.title};
    font-weight: 800;
    margin: 1rem auto;
    color: ${props => props.theme.colors.highlight};
  }
  p {
    font-family: ${props => props.theme.fonts.text};
    margin-bottom: 1rem;
    line-height: 1.25;
    font-weight: normal;
    color: ${props => props.theme.colors.inverted};
  }
  a,
  a span {
    font-family: ${props => props.theme.fonts.text};
    font-weight: normal;
    transition: all 0.2s;
    display: block;
    line-height: 1.15;
    border: none;
    &.inline {
      font-weight: 700;
      display: inline;
    }
    color: ${props => props.theme.colors.inverted};
    &:visited {
      color: ${props => props.theme.colors.inverted};
    }
    &:hover {
      color: ${props => props.theme.colors.highlight};
    }
  }
`

const Footer = () => {
  return (
    <Wrapper>
      {typeof window !== 'undefined' &&
      <ParticleComponent
        width="100%"
        id={'particle-wrapper-header'}
        params={particleConfig}
        style={{
          opacity: 0.6,
          zIndex: 1,
          backgroundColor: 'transparent',
          position: 'absolute',
          width: '100%',
          height: 'auto',
          top: 0
        }}
      />
      }
      <Container>
        <List>
          <Item>
            <h4>Kontakt</h4>
            <p>
              Vermflex GmbH
              <br />
              Gaiserwaldstrasse 15
              <br />
              9015 St. Gallen
            </p>
            <p>
              +41 71 477 44 00
              <br />
              info@vermflex.ch
            </p>
            <a href="tel:0041714774400" className="inline">
              <StyledButton className="ui circular icon button">
                <i className="icon phone" />
              </StyledButton>
            </a>
            <a href="mailto:info@vermflex.ch" className="inline">
              <StyledButton className="ui circular icon button">
                <i className="icon envelope" />
              </StyledButton>
            </a>
          </Item>
          <Item>
            <h4>Übersicht</h4>
            <Link to="/handwerk">Handwerk</Link>
            <Link to="/handwerk/drohnen">Drohnenvermessung</Link>
            <Link to="/handwerk/digitale-baustelle">Digitale Baustelle</Link>
            <Link to="/handwerk/monitoring">Mannschaft</Link>
            <Link to="/handwerk/BIM">BIM</Link>
            <Link to="/mannschaft">Mannschaft</Link>
            <Link to="/kunden">Kunden</Link>
            <Link to="/preise">Preise</Link>
            <Link to="/kontakt">Kontakt</Link>
            <Link to="/impressum-und-datenschutz">
              Impressum &
              <br />
              Datenschutzerklärung
            </Link>
          </Item>
          <Item>
            <Link to="/preise">
              <h4>Preisrechner</h4>
              Mit nur fünf Klicks
              <br />
              zu Ihrer Offerte:
              <br />
              dank unserem
              <br />
              Sofort-Preisrechner
            </Link>
          </Item>
          <Item>
            <h4>Soziale Medien</h4>
            <div
              href="https://www.instagram.com/cocoonvans/"
              rel="nofollow noopener noreferrer"
              target="_blank"
            >
              <a
                href="https://www.facebook.com/vermflex/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="ui icon facebook" />
{' '}
Facebook
</a>
              <a
                href="https://www.linkedin.com/company/vermflex/about/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="ui icon linkedin" />
{' '}
LinkedIn
</a>
              <a
                href="https://www.youtube.com/channel/UCZ1ImdR8MhakWsNBGWuIC4A"
                rel="noopener noreferrer"
                target="_blank"
              >
                // eslint-disable-next-line react/jsx-one-expression-per-line
                <i className="ui icon youtube" />
{' '}
Youtube
</a>
            </div>
          </Item>
        </List>
      </Container>
    </Wrapper>
  )
}

export default Footer
