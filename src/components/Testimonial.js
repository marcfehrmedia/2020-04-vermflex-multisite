import React  from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'
// import TestimonialBody from './TestimonialBody'

const StyledDiv = styled.div`
  text-align: left;
  padding: 0 1rem;
  h3.testimonial-text {
    color: ${props => props.theme.colors.secondary};
    font-family: ${props => props.theme.fonts.title};
    font-size: 1.2em;
    padding: 0 0 1rem 0;
    font-weight: 800;
    line-height: 1.4;
    color: ${props => props.theme.colors.secondary}
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 1em;
      line-height: 1.2;
    }
  }
  p {
    font-family: ${props => props.theme.fonts.text};
    font-weight: normal;
    color: ${props => props.theme.colors.secondary};
  }
  .testimonial-logo {
    text-align: left;
    margin: 1.5rem 0 0 0;
  }
`

const Testimonial = props => {
  return (
    <StyledDiv key={props.wrapperKey}>
      {props.data.node.text &&
      <h3 className={'testimonial-text'}>
        «{props.data.node.text.json.content[0].content[0].value}»
      </h3>
      }
      <p>{props.data.node.name}</p>
      <Img
        className="testimonial-logo"
        fluid={props.data.node.logoSmall.fluid}
        style={{ maxHeight: '5rem', maxWidth: '8rem', objectFit: 'contain', objectPosition: 'left top' }}
        imgStyle={{ objectFit: 'contain', objectPosition: 'left top' }}
      />
    </StyledDiv>
  )
}

export default Testimonial
