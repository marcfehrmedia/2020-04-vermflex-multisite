import { createGlobalStyle } from 'styled-components'

import AllerWoff from '../fonts/bup/aller-webfont.woff'
import AllerWoff2 from '../fonts/bup/aller-webfont.woff2'
import AllerBoldWoff from '../fonts/bup/aller-bold-webfont.woff'
import AllerBoldWoff2 from '../fonts/bup/aller-bold-webfont.woff2'

export default createGlobalStyle`
    @font-face {
        font-family: 'Aller';
        src: local('Font Name'), local('FontName'),
        url(${AllerWoff2}) format('woff2'),
        url(${AllerWoff}) format('woff');
        font-style: normal;
    },
    @font-face {
        font-family: 'AllerBold';
        src: local('Font Name'), local('FontName'),
        url(${AllerBoldWoff2}) format('woff2'),
        url(${AllerBoldWoff}) format('woff');
        font-style: normal;
    }
`